({
	initSession : function(component) {
        //check design mode
        console.log('initSession STARTED');
        if( window.location.href.includes('flexipageEditor')) {
            //not continue with logic, unable to select component and set up paramaters then
            component.set("v.errorMessage", 'DESIGN MODE');
            console.log('initSession FINISHED - design mode');
            return;
        }
        var replyPageURL = this.getPageURL();
        //alert("replyPageURL=" + replyPageURL);
        //return;
        var codeParam = this.getCodeURLParam("code");
        console.log('code param = ' + codeParam);
        if( codeParam && codeParam.length>0) {
        	this.validateCode(component, codeParam);
        }
        else {
            //do login to get code
            this.doLogin(component);
        }
	},
    
    refreshToken : function(component) {
        console.log('refreshToken STARTED');
        //get pbi_RefreshToken cookie value
        var cookieRefreshToken = this.getCookie('pbi_RefreshToken');
        console.log('Refresh cookie = ' + cookieRefreshToken );
        
        var action=component.get("c.refreshAccessToken");
        action.setParams({
            "parRefreshToken":component.get("v.refreshToken")
        }); 
        
        action.setCallback(this,function(res){
            console.log('refreshToken callback STARTED');
            var state = res.getState();  
            console.log(state);
            if(state=="SUCCESS"){
                var result = res.getReturnValue();
                console.log('Result='+result);
                component.set("v.errorMessage", result);
                //set token data from result
                var resultObj = JSON.parse(result);
                if( resultObj && resultObj.error ) {
                    component.set("v.errorMessage", 'ERROR: ' + resultObj.error + (resultObj.error_description?resultObj.error_description : '') );
                }
                else {
                    //should have token
                    if( resultObj ) {
                        component.set("v.accessToken", resultObj.access_token ? resultObj.access_token : '');
                    	component.set("v.refreshToken", resultObj.refresh_token ? resultObj.refresh_token : '');
                        component.set("v.expiresOn", resultObj.expires_on ? resultObj.expires_on : '');
                        
                        //set cookies & 
                        document.cookie="pbi_RefreshToken="+component.get("v.refreshToken");
                        document.cookie="pbi_AccessToken="+component.get("v.accessToken");
                        document.cookie="pbi_ExpiresOn="+component.get("v.expiresOn");
                        component.set("v.errorMessage",document.cookie);
                        //proceed with displaying
                        this.proceedWithDisplaying(component);
                    }
                   	
                }
            }
            
            console.log('refreshToken callback FINISHED');
        });
        
        $A.enqueueAction(action);
        console.log('refreshToken FINISHED');
    },
    
    proceedWithDisplaying: function(component) {
        console.log('proceedWithDisplaying STARTED');
    	//set cookies
        var accToken = this.getCookie('pbi_AccessToken');
        if( accToken && accToken.length > 0 ) {
            component.set("v.accessToken", accToken);
        }else {
            component.set("v.accessToken", '');
        }
        
        var refrToken = this.getCookie('pbi_RefreshToken');
        if( refrToken && refrToken.length > 0 ) {
            component.set("v.refreshToken", refrToken);
        }else {
            component.set("v.refreshToken", '');
        }
        var expOn = this.getCookie('pbi_ExpiresOn');
        if( expOn && expOn.length > 0 ) {
            component.set("v.expiresOn", expOn);
        }else {
            component.set("v.expiresOn", '');
        }
        
        //check if login required
        var expiresOn = parseInt(component.get("v.expiresOn"));
		var currentTime = Date.now()/1000;
        var accessToken = component.get("v.accessToken");
	    console.log('ExpiresOn='+expiresOn);
        console.log('currentTime='+currentTime);
        if( expiresOn <= currentTime || !accessToken || accessToken.length==0 ) {
            //do login
            console.log('proceedWithDisplaying do LOGIN');
            this.doLogin(component);
        }
        else {
            if((expiresOn - 2000) <= currentTime) {
                console.log('refresh TOKEN');
				this.refreshToken(component);
			}
            else {
                //render report
                this.renderReport(component);
            }
        }
        
        console.log('proceedWithDisplaying FINISHED');
	},
    
	validateCode : function(component, code) {
        console.log('validateCode STARTED');
        
        var action=component.get("c.validateCodeInApex");
        action.setParams({
            "parCode":code,
            "parRedirect_uri":this.getPageURL()
        }); 
        
        action.setCallback(this,function(res){
            console.log('validateCode callback STARTED');
            var state = res.getState();  
            console.log(state);
            if(state=="SUCCESS"){
                var result = res.getReturnValue();
                console.log('Result='+result);
                component.set("v.errorMessage", result);
                //set token data from result
                var resultObj = JSON.parse(result);
                if( resultObj && resultObj.error ) {
                    component.set("v.errorMessage", 'ERROR: ' + resultObj.error + (resultObj.error_description?resultObj.error_description : '') );
                    if(resultObj.error.includes('invalid_grant')) {
                        //get new code - reload page without code
                        location.href = this.getPageURL();
                        //location.href = "https://bravedo--powerbi.lightning.force.com/one/one.app";
                    }
                }
                else {
                    //should have token
                    if( resultObj ) {
                        component.set("v.accessToken", resultObj.access_token ? resultObj.access_token : '');
                    	component.set("v.refreshToken", resultObj.refresh_token ? resultObj.refresh_token : '');
                        component.set("v.expiresOn", resultObj.expires_on ? resultObj.expires_on : '');
                        
                        //set cookies & 
                        document.cookie="pbi_RefreshToken="+component.get("v.refreshToken");
                        document.cookie="pbi_AccessToken="+component.get("v.accessToken");
                        document.cookie="pbi_ExpiresOn="+component.get("v.expiresOn");
                        component.set("v.errorMessage",document.cookie);
                        //proceed with displaying
                        this.proceedWithDisplaying(component);
                    }
                   	
                }
            }
            
            console.log('validateCode callback FINISHED');
        });
        
        $A.enqueueAction(action);
        console.log('validateCode FINISHED');
    },
    
    renderReport : function(component) {
        console.log('renderReport STARTED');
        //get div element
        var query = 'div[data-comp-id="' + component.get("v.componentId") + '"] #PowerBiReportPlaceholder';
        console.log('renderReport ELEMENT QUERY = ' + query);
        var elemColl = document.querySelector( query );
        if( elemColl ) {
            console.log('renderReport RENDER ELEMENT FOUND');
            var embedConfiguration = {
            	type: 'report',
                id: component.get("v.reportId"),
                embedUrl: 'https://app.powerbi.com/reportEmbed',
                settings: {
                        filterPaneEnabled: true,
                        navContentPaneEnabled: true
                    }
            };
            console.log('renderReport EMBED CFG = ' + embedConfiguration);
            var element = elemColl;
            powerbi.accessToken = component.get("v.accessToken");
            console.log('renderReport EMBEDDING');
            var report = powerbi.embed(element, embedConfiguration);
        }
        else {
            console.log('renderReport RENDER ELEMENT NOT FOUND');
        }
        history.pushState({}, document.title, this.getPageURL() );
        console.log('renderReport FINISHED');
    },
    
    doLogin: function(component) {
        console.log('doLogin STARTED');
        var action=component.get("c.getAuthUrl");
        action.setParams({
            //"parCurrentURL":"https://bravedo--powerbi.lightning.force.com/one/one.app"
            "parCurrentURL": this.getPageURL()
        }); 
        action.setCallback(this,function(res){
            console.log('doLogin callback STARTED');
            var state = res.getState();  
            console.log(state);
            if(state=="SUCCESS"){
                var result = res.getReturnValue();
                console.log('Result='+result);
                component.set("v.authURL", result);
                //open login window - @TODO open in same window because unable to use window.opener
                //var loginWindow = window.open( component.get("v.authURL"),'Login','width=450,height=800,0,status=0');
                window.location.href = component.get("v.authURL");
            }
            
            console.log('doLogin callback FINISHED');
        });
        
        $A.enqueueAction(action);
        console.log('doLogin FINISHED');
    },
    
    getCodeURLParam: function(paramName) {
        console.log('getCodeURLParam STARTED');
        var retValue = '';
    	var sPageURL = decodeURIComponent(window.location.href); //You get the whole decoded URL of the page.
        console.log('getCodeURLParam sPageURL='+sPageURL);
        //separate by ?
        var questionPart = sPageURL.split('?');
        if( questionPart.length == 2 ) {
			var sURLVariables = questionPart[1].split('&');            
            for (var i = 0; i < sURLVariables.length; i++) {
                var sParameterName = sURLVariables[i].split('='); //to split the key from the value.
    
                if (sParameterName[0] === paramName) { 
                    retValue = sParameterName[1] === undefined ? '' : sParameterName[1];
                }
        	}
        }
        
        console.log('getCodeURLParam FINISHED');
        return retValue;
	},
    
    getCookie: function(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        console.log('ALL COOKIES=' + ca);
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    },
    
    getPageURL: function() {
        var prot = location.protocol;
        var host = location.host;
        var path = location.pathname
        var retValue = prot + '//' + host + path;
        console.log('getPageURL=' + retValue);
        return retValue;
    }
})