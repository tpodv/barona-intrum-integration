({
  doInit: function(cmp, event, helper) {
    var action = cmp.get("c.getContinuousRating");
    var accId = cmp.get("v.recordId");

    action.setParams({ accountId: accId });
    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var result = response.getReturnValue();
        cmp.set("v.resultMessage", result);
        console.log("Result:" + result);
      } else if (state === "INCOMPLETE") {
      } else if (state === "ERROR") {
        var errors = response.getError();
        if (errors) {
          if (errors[0] && errors[0].message) {
            console.log("Error message: " + errors[0].message);
            cmp.set("v.resultMessage", "Error message:  " + errors[0].message);
          }
        } else {
          console.log("Unknown error");
        }
      }
    });
    $A.enqueueAction(action);
  }
});
