trigger MDM_Opportunity_Deletion on Opportunity (after delete, after undelete) {
	List<MDM_Deleted_Object__c> deletedObjects = new List<MDM_Deleted_Object__c>();

	for(Opportunity c : (Trigger.isUndelete ? Trigger.new : Trigger.old)) {
    	deletedObjects.add(new MDM_Deleted_Object__c(Object_Id__c = c.Id, Object_Type__c = 'Opportunity', undeleted__c = Trigger.isUndelete));
  	}
 
  	if (!deletedObjects.isEmpty())
    	insert deletedObjects;
}