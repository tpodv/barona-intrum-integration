trigger changeApproveChecksumCheckboxState on BL_Info__c (before update) {
    for(BL_Info__c info : Trigger.new) {
        BL_Info__c old_info = Trigger.oldMap.get(info.Id);
        if(info.sonet_latest_checksum__c != old_info.sonet_latest_checksum__c && info.sonet_transfer_approved__c && info.sonet_latest_checksum__c != info.sonet_approval_checksum__c) {
            info.sonet_transfer_approved__c = false;
        } else if (!old_info.sonet_transfer_approved__c && info.sonet_transfer_approved__c) {
            info.sonet_approval_checksum__c = info.sonet_latest_checksum__c;
        }
    }
}