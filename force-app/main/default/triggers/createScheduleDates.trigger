/*This trigger will run if agreement type is Fixed term agreement.
 It will create Schedule Dates based on beginning and ending dates.
 For deleting old Schedule Date records, we invoke future method to avoid recursive call 
 because of roll-up summary fields on Agreement object.
*/

trigger createScheduleDates on Agreement__c (after insert,after update) {

           
               List<id> Agreementlist = new List<id>();  
               List<Schedule_Date__c> scheduleDates = new List<Schedule_Date__c>();
               List<Agreement__c> AgreementDeletelist = new List<Agreement__c>(); 
               List<Id> ScheduleDateDeletelist = new List<Id>();   
               List<Schedule_Date__c> ScheduleDateList = new List<Schedule_Date__c>();
               Map<Id,Agreement__c> AgreeMap = new Map<ID,Agreement__c>();
               
               for(Agreement__c Agreement: Trigger.new){
                   
                   if(Agreement.Type__c=='Fixed term agreement' || (Agreement.Beginning__c != null && Agreement.Ending__c != null )){  
                       if(trigger.isInsert){ 
                           Agreementlist.add(Agreement.id);              
                           AgreeMap.put(Agreement.id,Agreement);
                       } 
                       
                      // Runs only when night price or extra services night price or beginning or ending fields gets changed.
                      if(trigger.isUpdate){
                           Double oldCount = Trigger.oldMap.get(Agreement.id).Night_Price__c;
                           Double oldCount2 = Trigger.oldMap.get(Agreement.id).Extra_Services_Night_Price__c;
                           String TypeCheck = Trigger.oldMap.get(Agreement.id).Type__c;
                           date StartDate = Trigger.oldMap.get(Agreement.id).Beginning__c; 
                           date EndDate = Trigger.oldMap.get(Agreement.id).Ending__c; 
                           date newStartDate = Agreement.Beginning__c;
                           date newEndDate = Agreement.Ending__c;
                           if(newStartDate<newEndDate){
                               if(( Agreement.Night_Price__c != oldCount)||
                                 ( Agreement.Extra_Services_Night_Price__c != oldCount2 )||
                                 ( Agreement.Beginning__c != StartDate )||
                                 ( Agreement.Ending__c!= EndDate )) {
                                      Agreementlist.add(Agreement.id);
                                      ScheduleDateDeletelist.add(Agreement.id);     //Record Ids need to be deleted
                                      AgreeMap.put(Agreement.id,Agreement);
                               }    
                           }
                       }
                   }           
               }
            
               //Set Status of ScheduleDateDeletelist Ids to true for deletion
               if(ScheduleDateDeletelist.size() >0){
                   List<Schedule_Date__c> sList = new List<Schedule_Date__c>();
                   scheduleDates = [Select id,Status_Check__c from Schedule_Date__c where Accommodation_Agreement__c IN:ScheduleDateDeletelist ];
                   for(integer i= 0;i<scheduleDates.size();i++){
                       scheduleDates[i].Status_Check__c = true;
                        sList.add(scheduleDates[i]);
                   }
                   update sList;
                   Datetime sysTime = System.now();
                   sysTime = sysTime.addSeconds(20);
                   String chron_exp = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
                   scheduleFutureClassDeleteDates sched = new scheduleFutureClassDeleteDates();
                   System.schedule('Future class delete Schedule Dates ' + sysTime.getTime(),chron_exp,sched);   //Invoke future class.
               }
              
                
               if(Agreementlist.size() > 0){ 
                    //Iterate through Agreements to create schedule dates    
                   for(Id aId: Agreementlist){
                       Agreement__c Agr  = AgreeMap.get(aId);
                       date dt = agr.Beginning__c;
                        
                       //Create schedule date records equal to number of days between beginning and ending dates. 
                       while(dt < agr.Ending__c){
                           if((agr.Extra_Services_Night_Price__c!=null)||(agr.Night_Price__c!=null)){
                               Schedule_Date__c SchDateService = new Schedule_Date__c();
                               if(agr.Extra_Services_Night_Price__c!=null)
                                   SchDateService.Extra_Service_Amount__c = agr.Extra_Services_Night_Price__c;
                               if(agr.Night_Price__c!=null)
                                   SchDateService.Daily_Amount__c = agr.Night_Price__c;
                               SchDateService.CurrencyIsoCode = agr.CurrencyIsoCode;    
                               SchDateService.Accommodation_Agreement__c = agr.ID;
                               SchDateService.Date__c = dt;
                               ScheduleDateList.add(SchDateService);   
                           }

                           dt = dt.adddays(1);
                       }
                   } 
              
                   try{
                       if(ScheduleDateList.size()>0)  
                           insert ScheduleDateList; 
                   }
                   catch(Exception e){
                       AgreeMap.get(Agreementlist[0]).addError('Some problem has occurred while creating schedule dates. '+e);
                   }
                          
               }

}