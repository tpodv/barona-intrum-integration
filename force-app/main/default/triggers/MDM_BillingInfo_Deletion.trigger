trigger MDM_BillingInfo_Deletion on BL_Info__c (after delete, after undelete) {
	List<MDM_Deleted_Object__c> deletedObjects = new List<MDM_Deleted_Object__c>();

	for(BL_Info__c c : (Trigger.isUndelete ? Trigger.new : Trigger.old)) {
    	deletedObjects.add(new MDM_Deleted_Object__c(Object_Id__c = c.Id, Object_Type__c = 'BL_Info__c', undeleted__c = Trigger.isUndelete));
  	}
 
  	if (!deletedObjects.isEmpty())
    	insert deletedObjects;
}