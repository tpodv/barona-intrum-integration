trigger AccountUpdateTriggerExtUpdate on Account (before update, before insert, after update) {
     new BaronaAccountTriggerDispatcher().accountUpdateExt(Trigger.New, Trigger.OldMap);

}