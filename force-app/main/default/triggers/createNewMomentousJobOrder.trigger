/**
 *
 * createNewMomentousJobOrder Trigger
 * Created at BiiT Oy
 *
 * - 04/Jan/2012 - Created
 * - 09/Jan/2012 - Removed non-required fields on Job Order creation.
 * - 24/Jan/2012 - Added Amount to Job Order from Opportunity
 * - 29/Feb/2012 - Cleanup + Fixes to fit new Job Order required fields.
 *
 * Trigger that creates a new Job Order only once, and after update
 * the related opportunity. If the RecordType is 'Momentous' and 
 * the Opportunity was won. The trigger covers only the required fields. 
 * The Job Order created is related with the Opportunity Won.
 * 
 * To control that the trigger fires only once, there should exist
 * a workflow rule that updates the flag WasJobOrderCreated__c.
 *
 * @author Manuel Gonzalez
 * @FIXME: For some reason, the indentation when creating a new Job Order
 *         makes the test process yell with lines not covered. Salesforce bug?
 *
 */
trigger createNewMomentousJobOrder on Opportunity (after update) {
/*    
    Id MomentousRecordType = [SELECT Id
                              FROM RecordType
                              WHERE Name=:'Momentous'
                              AND SOBJECTTYPE=:'ts2__Job__c'].Id;
    
    //Default recruiter is the first on the list with Momentous role.
    List<User> defaultRecruiter = [SELECT Id 
                                   FROM User
                                   WHERE UserRoleId IN      
                                                       (SELECT Id
                                                        FROM UserRole 
                                                        WHERE Name=:'Momentous')
                                    LIMIT 1];                             
    
    for(Opportunity opp: Trigger.new){
        //If the Job Order was not created yet, the stage is Won and the Industry is "Momentous" then create the Job Order.
        //To update the flag WasJobOrderCreated__c there should exist a Workflow rule that handles this. 
        if(opp.WasJobOrderCreated__c == false && opp.StageName=='Voitettu tarjous' && opp.Toimiala__c=='Momentous'){
            
            ts2__Job__c jb = new ts2__Job__c (RecordTypeId = MomentousRecordType, Name = opp.Name+' - '+opp.Name_of_the_position__c, Ammount__c = opp.Amount, Opportunity__c = opp.Id, ts2__Account__c = opp.AccountId, ts2__Recruiter__c = defaultRecruiter[0].Id, ts2__Status__c = 'Open', ts2__Job_Function__c = 'Training', ts2__Department__c = 'Professional Service');
                              
            insert jb;

        }
    }
*/
}