trigger MDM_Account_Deletion on Account (after delete, after undelete) {
/*
 * 
 * Creator - Unknown
 * Modifications :
 * 07.03.2019 - Paul Mc Namara paul.mcnamara@biit.fi - Add publish to platform event on Account Deletion
 */     
    
     new BaronaAccountTriggerDispatcher().deleteAccountTrigger(Trigger.New, Trigger.Old);

}