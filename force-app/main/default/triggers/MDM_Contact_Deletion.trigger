trigger MDM_Contact_Deletion on Contact (after delete, after undelete) {
      new BaronaContactTriggerDispatcher().deleteContactTrigger(Trigger.New, Trigger.Old);            
}