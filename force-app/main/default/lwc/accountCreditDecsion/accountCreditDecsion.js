import {
  LightningElement,
  api,
  track,
  wire
} from "lwc";
import cancelContinuousRating from "@salesforce/apex/CISProductsIntegrationOutbound.cancelContinuousRating";
import getAccountRatings from "@salesforce/apex/CISProductsIntegrationOutbound.getAccountRatings";
import getExpressDecision from "@salesforce/apex/CISProductsIntegrationOutbound.getExpressCreditDecision";
import getContinuousDecision from "@salesforce/apex/CISProductsIntegrationOutbound.getContinuousCreditDecisionAN";
import isCancellationAllowed from "@salesforce/apex/CISProductsIntegrationOutbound.isCancelAllowed";
import {
  refreshApex
} from "@salesforce/apex";
import {
  ShowToastEvent
} from "lightning/platformShowToastEvent";
export default class AccountContinuousRating extends LightningElement {
  @api recordId;
  @track accountRatings;
  @track ratingLoaded = false;
  @track isRunning = false;
  @track openmodal = false;
  wiredDecisionResult;

  @wire(isCancellationAllowed)
  isCancelAllowed;

  _title = "Get Credit Decision";
  variantOptions = [{
      label: "error",
      value: "error"
    },
    {
      label: "warning",
      value: "warning"
    },
    {
      label: "success",
      value: "success"
    },
    {
      label: "info",
      value: "info"
    }
  ];
  // Get account credit decisions / ratings
  @wire(getAccountRatings, {
    accountId: "$recordId"
  })
  imperativeWiring(result) {
    this.wiredDecisionResult = result;
    if (result.data) {
      this.accountRatings = JSON.parse(result.data);
      console.log(
        "account ratings data=" + JSON.stringify(this.accountRatings)
      );
      this.error = undefined;
      this.ratingLoaded = true;
    } else if (result.error) {
      this.error = result.error;
      console.log("error=" + JSON.stringify(error));
      this.records = undefined;
    }
  }

  handleGetExpressDecisionClick(event) {
    const action = event.detail.name;
    this.isRunning = true;
    getExpressDecision({
        accountId: this.recordId
      })
      .then(result => {
        console.log("ExpressDecision Request sent. Response= " + result);
        var response = JSON.parse(result);
        this.msg = response.statusMessage;
        this.variant = response.statusCode === 200 ? "success" : "error";
        this.isRunning = false;
        refreshApex(this.wiredDecisionResult);
        this.showNotification();
      })
      .catch(error => {
        this.isRunning = false;
        this.message =
          "Error received: code" +
          error.errorCode +
          ", " +
          "message " +
          error.body.message;
        console.log("Save error=");
        this.msg = "Contunous Rating was not updated. " + error.body.message;
        this.variant = "error";
        this.showNotification();
      });
  }

  handleGetContinuousDecisionClick(event) {
    const action = event.detail.name;
    this.isRunning = true;
    getContinuousDecision({
        accountId: this.recordId
      })
      .then(result => {
        console.log("ContinuousDecision Request sent. Response= " + result);
        var response = JSON.parse(result);
        this.msg = response.statusMessage;
        this.variant = response.statusCode === 200 ? "success" : "error";
        this.isRunning = false;
        refreshApex(this.wiredDecisionResult);
        this.showNotification();
      })
      .catch(error => {
        this.isRunning = false;
        this.message =
          "Error received: code" +
          error.errorCode +
          ", " +
          "message " +
          error.body.message;
        console.log("Save error=");
        this.msg = "Contunous Rating was not updated. " + error.body.message;
        this.variant = "error";
        this.showNotification();
      });
  }

  handleCancelRatingClick(event) {
    this.isRunning = true;
    cancelContinuousRating({
        assignmentNumber: this.assignmentNumber
      })
      .then(result => {
        console.log("Cancel Request response=" + JSON.stringify(result));
        var response = JSON.parse(result);
        console.log("Cancel Request result=" + response.statusMessage);
        this.msg = response.statusMessage;
        this.variant = response.statusCode === 200 ? "success" : "error";
        this.isRunning = false;
        refreshApex(this.wiredDecisionResult);
        this.showNotification();
      })
      .catch(error => {
        this.isRunning = false;
        console.log("Save error=" + JSON.stringify(error));
        if (error.body) {
          console.log("Inside cancel rating error=" + JSON.stringify(error));
          this.message =
            "Error received: code" +
            error.errorCode +
            ", " +
            "message " +
            error.body.message;
          this.msg = "Contunous Rating was not canceled. " + error.body.message;
          this.variant = "error";
          this.showNotification();
        }
      });
  }

  get accountHasContinuousDecision() {
    return this.ratingLoaded && this.accountRatings.continuousDecision !== null;
  }

  get accountHasExpressDecision() {
    return this.ratingLoaded && this.accountRatings.expressDecision !== null;
  }

  get accountHasCreditLimit() {
    return this.accountRatings.creditLimit !== null;
  }

  showNotification() {
    const evt = new ShowToastEvent({
      title: this._title,
      message: this.msg,
      variant: this.variant
    });
    this.dispatchEvent(evt);
  }

  openLimitModal() {
    this.openmodal = true;
  }

  closeModal() {
    this.openmodal = false;
  }

  saveMethod() {
    this.isRunning = true;
    this.template.querySelector("[data-field='limit']").focus();
    var creditLimit = this.template.querySelector("[data-field='limit']").value;
    if (creditLimit !== "" && creditLimit >= 1 && creditLimit <= 99999999) {
      this.closeModal();
      getContinuousDecision({
          accountId: this.recordId,
          creditLimit: creditLimit
        })
        .then(result => {
          console.log("ContinuousDecision Request sent. Response= " + result);
          var response = JSON.parse(result);
          this.msg = response.statusMessage;
          this.variant = response.statusCode === 200 ? "success" : "error";
          this.isRunning = false;
          refreshApex(this.wiredDecisionResult);
          this.showNotification();
        })
        .catch(error => {
          this.isRunning = false;
          this.message =
            "Error received: code" +
            error.errorCode +
            ", " +
            "message " +
            error.body.message;
          console.log("Save error=");
          this.msg = "Continuous Decision was not updated. " + error.body.message;
          this.variant = "error";
          this.showNotification();
        });
    }
  }

  get CancelAllowed() {
    return this.isCancelAllowed.data;
  }

  get isLoaded() {
    return this.ratingLoaded && this.isCancelAllowed.data !== undefined;
  }
}