import {
  LightningElement,
  api,
  track,
  wire
} from "lwc";
import creditLimitAmount from '@salesforce/label/c.ContinuousRating_CreditLimitAmount';
import getAccountRatings from "@salesforce/apex/CISProductsIntegrationOutbound.getAccountRatings";
import getContinuousRating from "@salesforce/apex/CISProductsIntegrationOutbound.getContinuousRating";
import cancelContinuousRating from "@salesforce/apex/CISProductsIntegrationOutbound.cancelContinuousRating";
import isCancellationAllowed from "@salesforce/apex/CISProductsIntegrationOutbound.isCancelAllowed";

import {
  refreshApex
} from "@salesforce/apex";
import {
  ShowToastEvent
} from "lightning/platformShowToastEvent";
export default class AccountContinuousRating extends LightningElement {
  @api recordId;
  @track accountRatings;
  @track ratingLoaded = false;
  @track isRunning = false;
  wiredRatingResult;
  assignmentNumber;

  label = {
    creditLimitAmount
  };

  _title = "Get Continuous Rating";
  variantOptions = [{
      label: "error",
      value: "error"
    },
    {
      label: "warning",
      value: "warning"
    },
    {
      label: "success",
      value: "success"
    },
    {
      label: "info",
      value: "info"
    }
  ];

  @wire(isCancellationAllowed)
  isCancelAllowed;

  // Get account credit decisions / ratings
  @wire(getAccountRatings, {
    accountId: "$recordId"
  })
  imperativeWiring(result) {
    this.wiredRatingResult = result;
    if (result.data) {
      this.accountRatings = JSON.parse(result.data);
      if (this.accountRatings.continuousRating)
        this.assignmentNumber = this.accountRatings.continuousRating.assignmentNumber;
      console.log(
        "account ratings data=" + JSON.stringify(this.accountRatings)
      );
      this.error = undefined;
      this.ratingLoaded = true;
    } else if (result.error) {
      this.error = result.error;
      console.log("error=" + JSON.stringify(error));
      this.records = undefined;
    }
  }

  handleGetRatingClick(event) {
    const action = event.detail.name;
    this.isRunning = true;
    getContinuousRating({
        accountId: this.recordId
      })
      .then(result => {
        console.log("Request sent");
        var response = JSON.parse(result);
        this.msg = response.statusMessage;
        this.variant = response.statusCode === 200 ? "success" : "error";
        this.isRunning = false;
        refreshApex(this.wiredRatingResult);
        this.showNotification();
      })
      .catch(error => {
        this.isRunning = false;
        this.message =
          "Error received: code" +
          error.errorCode +
          ", " +
          "message " +
          error.body.message;
        console.log("Save error=");
        this.msg = "Contunous Rating was not updated. " + error.body.message;
        this.variant = "error";
        this.showNotification();
      });
  }

  handleCancelRatingClick(event) {
    this.isRunning = true;
    cancelContinuousRating({
        assignmentNumber: this.assignmentNumber
      })
      .then(result => {
        console.log("Cancel Request response=" + JSON.stringify(result));
        var response = JSON.parse(result);
        console.log("Cancel Request result=" + response.statusMessage);
        this.msg = response.statusMessage;
        this.variant = response.statusCode === 200 ? "success" : "error";
        this.isRunning = false;
        refreshApex(this.wiredRatingResult);
        this.showNotification();
      })
      .catch(error => {
        this.isRunning = false;
        console.log("Save error=" + JSON.stringify(error));
        if (error.body) {
          console.log("Inside cancel rating error=" + JSON.stringify(error));
          this.message =
            "Error received: code" +
            error.errorCode +
            ", " +
            "message " +
            error.body.message;
          this.msg = "Contunous Rating was not canceled. " + error.body.message;
          this.variant = "error";
          this.showNotification();
        }
      });
  }

  get accountHasRating() {
    let hasRating = this.ratingLoaded && this.accountRatings.continuousRating !== null &&
      this.accountRatings.continuousRating.assignmentNumber !== null;
    return hasRating;
  }

  get accountHasCreditLimit() {
    return this.accountRatings.creditLimit !== null;
  }

  showNotification() {
    const evt = new ShowToastEvent({
      title: this._title,
      message: this.msg,
      variant: this.variant
    });
    this.dispatchEvent(evt);
  }

  get CancelAllowed() {
    return this.isCancelAllowed.data;
  }

  get isLoaded() {
    return this.ratingLoaded && this.isCancelAllowed.data !== undefined;
  }

  get creditLimitEndDate() {
    var endDate = this.accountRatings.creditLimitEndDate !== undefined ? this.accountRatings.creditLimitEndDate : null;
    return endDate !== null ? endDate : 'N/A';
  }
}