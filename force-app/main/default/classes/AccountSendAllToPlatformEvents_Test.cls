/**
 * AccountSendAllToPlatformEvents_Test
 *
 * Test class for AccountSendAllToPlatformEvents.
 *
 * Revision History:
 * 28.02.2019 paul.mcnamara@biit.fi  - Created.
 *
 */

@isTest
public class AccountSendAllToPlatformEvents_Test {
 @testSetup
    static void setup() {
        // Create data for the test
        List<Account> accs = new List<Account>();
        // insert 200 accounts
        for (Integer i=0;i<200;i++) {
            accs.add(new Account(Name='Acc '+i,
                BillingStreet='Mannerheimintie '+i, MDM_Id__c='MDM00'+i));
        }
        insert accs;
    }
 
    static testmethod void test() { 
        Test.startTest();
        AccountSendAllToPlatformEvents ac = new AccountSendAllToPlatformEvents();
        Id batchId = Database.executeBatch(ac);
        Test.stopTest();
    }
}