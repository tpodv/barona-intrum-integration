global class CISProductsContinuousRatingUpdate implements Schedulable {

  private static integer PAGE_SIZE = 2000;

  //Continuous Rating Mass Updates
  public static void getContinuousRatingUpdates(){
    string service = 'continuousRating/updates?markAsRetrieved=true?pageSize=' + PAGE_SIZE + '?pageIndex=0';
    HttpResponse response = CISProductsIntegrationOutbound.getCISProductResponse('GET', service, '');
    if(response.getStatusCode() != 200 ) {
      system.debug('Continuous Rating Updates failed. Error: '+response.getStatusCode()+', '+response.getStatus());
    }else {
      string jsonResponse = response.getBody();
      if( updateCreditDecisions(jsonResponse))
        scheduleContinuousRatingUpdates();
    }
  }

  private static void scheduleContinuousRatingUpdates(){
    Datetime sysTime = System.now();
    integer minute = sysTime.minute();
    integer hour = sysTime.hour();
    integer day = sysTime.day();
    integer month = sysTime.month();
    integer year = sysTime.year();
    minute = minute + 15;
    String chron_exp = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ? ' + year;
    if(!Test.isRunningTest())
      System.schedule('Every 15 minutes', chron_exp, new CISProductsContinuousRatingUpdate() );
  }

  public static boolean updateCreditDecisions(string jsonResponse){
    Map<Integer,CISProductsIntegrationOutbound.creditDecision> creditDecisionMap = new Map<Integer, CISProductsIntegrationOutbound.creditDecision>();
    Set<Integer> assignmentNumberSet;
    Map<String, Object> root = (Map<String, Object>)JSON.deserializeUntyped(jsonResponse);
    List<Object> creditDecisionLst = (List<Object>)root.get('continuousRatingStatuses ');
    Integer recordCount = (Integer)root.get('totalRecords ');
    for (Object item : creditDecisionLst) {
      Map<String, Object> objectMap = (Map<String, Object>)item;
      String rec = (String)JSON.serialize(objectMap);
      CISProductsIntegrationOutbound.creditDecision decision =
        (CISProductsIntegrationOutbound.creditDecision)JSON.deserialize(rec, CISProductsIntegrationOutbound.creditDecision.class);
      integer assignmentNum = (integer)decision.assignmentNumber;
      creditDecisionMap.put(assignmentNum, decision);
      assignmentNumberSet.add(assignmentNum);
    }
    CISProductsContinuousRatingUpdateBatch bcn = new CISProductsContinuousRatingUpdateBatch(assignmentNumberSet,creditDecisionMap);
    ID batchprocessid = Database.executeBatch(bcn, 200);
    return recordCount == PAGE_SIZE;
  }

  global void execute(SchedulableContext sc){}

}
