public class CISProductsContinuousRatingUpdateBatch implements Database.Batchable<sObject> {

  private Map<Integer,CISProductsIntegrationOutbound.creditDecision> creditRatingsMap;
  private Set<Integer> assignNumSet;

  public CISProductsContinuousRatingUpdateBatch(Set<Integer> assignmentNumbers,
                                                Map<Integer,CISProductsIntegrationOutbound.creditDecision> creditRatings){
    assignNumSet = assignmentNumbers;
    creditRatingsMap = creditRatings;
  }

  public Database.QueryLocator start(Database.BatchableContext bc) {

    String soql = 'SELECT Id, CreditRating__c, RecommendedCreditLimit__c, '+
                  'RecalculationReason__c, RecommendationDate__c, '+
                  'AssignmentNumber__c, Reason__c '+
                  'FROM CreditDecision__c WHERE AssignmentNumber__c IN assignNumSet'+
                  (Test.isRunningTest() ? ' LIMIT 50' : '');
    return Database.getQueryLocator(soql);
  }

  public void execute(Database.BatchableContext bc, List<CreditDecision__c> records) {
    try{
      for( CreditDecision__c rec : records ) {
        Integer assgnNum = (Integer)rec.AssignmentNumber__c;
        CISProductsIntegrationOutbound.creditDecision creditDecision = creditRatingsMap.get(assgnNum);
        rec.CreditRating__c = creditDecision.creditRating;
        rec.RecommendedCreditLimit__c = creditDecision.recommendedCreditLimit;
        rec.RecalculationReason__c = creditDecision.recalculationReason;
        rec.RecommendationDate__c = creditDecision.notificationDate;
        rec.Reason__c = creditDecision.recommendationReason;
      }
      update records;
    }catch(Exception e) {
      system.debug('Error mass updating Continuous Credit Ratings. '+e.getMessage());
    }

  }

  public void finish(Database.BatchableContext bc) {}
}
