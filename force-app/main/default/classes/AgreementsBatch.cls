global class AgreementsBatch implements Database.Batchable<sObject>{

global final String query;
global Map<String,Agreement__c> mapAgreementsToUpdate;
global Map<String,Agreement__c> mapAgreementsToInsert;
global Map<String,Contact> mapContactsToInsert;
global Map<String,Account> mapAccountsToInsert;
global static final Id AccRecType = [SELECT Id
                                     FROM RecordType
                                     WHERE Name =:'Organization'
                                     AND SOBJECTTYPE =:'Account'].Id;

    global AgreementsBatch(String q){
    
        query = q;
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Unprocessed_Agreement__c> listUA){
    
        List<Unprocessed_Agreement__c> unprocessedAgreementsToUpdate = new List<Unprocessed_Agreement__c>();
        mapAgreementsToUpdate = new Map<String,Agreement__c>();
        mapAgreementsToInsert = new Map<String,Agreement__c>();
        mapContactsToInsert = new Map<String,Contact>();
        mapAccountsToInsert = new Map<String,Account>();
        Map<Id,Unprocessed_Agreement__c> toBeUpdatedMap = new Map<Id,Unprocessed_Agreement__c>();
            
        List<String> unprocessedNamesList = new List<String>();
        List<String> unprocessedContactEmailsList = new List<String>();
        List<String> unprocessedCustomerIdsList = new List<String>();
        for(Unprocessed_Agreement__c u:listUA){
            unprocessedNamesList.add(u.Agreement_ID__c);
            unprocessedContactEmailsList.add(u.Contact_Email__c);
            unprocessedCustomerIdsList.add(u.Customer_ID__c);
        }
        List<Agreement__c> agreementsList = new List<Agreement__c>([SELECT Name,
                                                                       Apartment_Postal_Code__c,
                                                                       Beginning__c,
                                                                       Country_Code__c,
                                                                       CurrencyIsoCode,
                                                                       ISA_Days__c,
                                                                       Ending__c,
                                                                       Extra_Services_Night_Price__c,
                                                                       Night_Price__c,
                                                                       Office__c,
                                                                       One_Time_Price__c,
                                                                       Contact_Email__c,
                                                                       Contact_Mobile__c,
                                                                       Contact_Name__c
                                                                    FROM Agreement__c
                                                                    WHERE Name IN:unprocessedNamesList]);
        List<Contact> contactsList = new List<Contact>([SELECT Id, 
                                                               Email 
                                                        FROM Contact 
                                                        WHERE Email IN:unprocessedContactEmailsList]);
        List<Account> accountsList = new List<Account>([SELECT Name,
                                                            Y_tunnus__c
                                                        FROM Account
                                                        WHERE Y_tunnus__c IN:unprocessedCustomerIdsList]);                                                                                                                                                                              
        //Map of Agreements by Name
        Map<String,Agreement__c> agreementsMap = new Map<String,Agreement__c>();
        for(Agreement__c a:agreementsList){
            agreementsMap.put(a.Name,a);
        }
        //Map of Contacts by Email
        Map<String,Contact> contactsMap = new Map<String,Contact>();
        for(Contact c:contactsList){
            contactsMap.put(c.Email,c);
        }
        //Map of Accounts by Y-tunnus
        Map<String,Account> accountsMap = new Map<String,Account>();
        for(Account a:accountsList){
            accountsMap.put(a.Y_tunnus__c,a);
        }                                                           

        //Main loop
        for(Unprocessed_Agreement__c unprocessed:listUA){
          try {
            
            if(accountsMap.containskey(unprocessed.Customer_ID__c) ||
                mapAccountsToInsert.containskey(unprocessed.Customer_ID__c)){
                //Update Account here if needed
            }else{
                try {
                    createAccount(unprocessed);
                    }
                catch(Exception e) {
                    throw e;
                    continue;
                } 
            }
            
            if(contactsMap.containskey(unprocessed.Contact_Email__c) ||
                mapContactsToInsert.containskey(unprocessed.Contact_Email__c)){
                //Update Contact here if needed
            }else{
                Account relatedAccount = accountsMap.get(unprocessed.Customer_ID__c);
                if(relatedAccount == null){
                    relatedAccount = mapAccountsToInsert.get(unprocessed.Customer_ID__c);
                }
                try {
                    createContact(unprocessed, relatedAccount);
                    }
                catch(DmlException e) {
                  //throw e;
                }    
            }
        
            if(agreementsMap.containskey(unprocessed.Agreement_ID__c) ||
                mapAgreementsToInsert.containskey(unprocessed.Agreement_ID__c) ||
                mapAgreementsToUpdate.containskey(unprocessed.Agreement_ID__c)){
                try {
                    updateAgreement(agreementsMap.get(unprocessed.Agreement_ID__c),unprocessed);
                    }
                catch(Exception e) {
                     throw e;
                }
            }else{
                Account relatedAccount = accountsMap.get(unprocessed.Customer_ID__c);
                if(relatedAccount == null){
                    relatedAccount = mapAccountsToInsert.get(unprocessed.Customer_ID__c);   
                }
                try {
                    createAgreement(unprocessed, relatedAccount);
                    }
                catch(Exception e) {
                   throw e;
                }     
            }   
        
        
        try {
            createChatterNotifications(mapAgreementsToInsert);
            }
       catch(Exception e) {
        //ignore chatter errors
            }
    
        } //try
        catch(Exception e) {
           unprocessed.error_message__c= e.getMessage();
           unprocessedAgreementsToUpdate.add(unprocessed);
           toBeUpdatedMap.put(unprocessed.Id,unprocessed);
         }
        }
        //system.debug('UNPROCESSED AGREEMENT ERROR COUNT ='+unprocessedAgreementsToUpdate.size());
         update unprocessedAgreementsToUpdate;
         //System.debug('***listUa size ='+listUa.size());
         List<Unprocessed_Agreement__c> toBeDeletedList = new List<Unprocessed_Agreement__c>();
         
         for(Unprocessed_Agreement__c ua:listUa) {
           if(toBeUpdatedMap.containskey(ua.Id)==false) {
               toBeDeletedList.add(ua);
               }
         }

        // System.debug('***toBeDeleted list size ='+toBeDeletedList.size());
        delete toBeDeletedList;
  }  
    global void createAgreement(Unprocessed_Agreement__c unprocessed, Account acc){
        
        Agreement__c newAgreement = new Agreement__c(Name = unprocessed.Agreement_ID__c,
                                                        Account__c = acc.Id,
                                                        Apartment_Postal_Code__c = unprocessed.Apartment_Postal_Code__c,
                                                        Beginning__c = unprocessed.Beginning__c,
                                                        Country_Code__c = unprocessed.Country_Code__c,
                                                        CurrencyIsoCode = unprocessed.CurrencyIsoCode,
                                                        ISA_Days__c = unprocessed.ISA_Days__c,
                                                        Ending__c = unprocessed.Ending__c,
                                                        Extra_Services_Night_Price__c = unprocessed.Extra_Services_Night_Price__c,
                                                        Night_Price__c = unprocessed.Night_Price__c,
                                                        Office__c = unprocessed.Office__c,
                                                        One_Time_Price__c = unprocessed.One_Time_Price__c,
                                                        Contact_Email__c = unprocessed.Contact_Email__c,
                                                        Contact_Mobile__c = unprocessed.Mobile__c,
                                                        Contact_Name__c = unprocessed.Contact_Name__c);
        insert newAgreement;
        mapAgreementsToInsert.put(newAgreement.Name,newAgreement);                                                              
    }
    
    global void updateAgreement(Agreement__c agreement, Unprocessed_Agreement__c unprocessed){
    
        agreement.Apartment_Postal_Code__c = unprocessed.Apartment_Postal_Code__c;
        agreement.Beginning__c = unprocessed.Beginning__c;
        agreement.Country_Code__c = unprocessed.Country_Code__c;
        agreement.ISA_Days__c = unprocessed.ISA_Days__c; 
        agreement.Ending__c = unprocessed.Ending__c;
        agreement.Extra_Services_Night_Price__c = unprocessed.Extra_Services_Night_Price__c;
        agreement.Night_Price__c = unprocessed.Night_Price__c;
        agreement.Office__c = unprocessed.Office__c;
        agreement.One_Time_Price__c = unprocessed.One_Time_Price__c;
        agreement.Contact_Email__c = unprocessed.Contact_Email__c;
        agreement.Contact_Mobile__c = unprocessed.Mobile__c;
        agreement.Contact_Name__c = unprocessed.Contact_Name__c;
        update agreement;
        mapAgreementsToUpdate.put(agreement.Name,agreement);
    }
    
    global void createContact(Unprocessed_Agreement__c unprocessed, Account acc){

        if(unprocessed.Contact_Email__c !='' && 
            unprocessed.Contact_Name__c != ''){
  
            Contact newContact = new Contact(AccountId = acc.Id,
                                            LastName= unprocessed.Contact_Name__c,
                                            MailingStreet = unprocessed.Customer_Address__c,
                                            MailingPostalCode = unprocessed.Customer_Postal_Code__c,
                                            MobilePhone = unprocessed.Mobile__c,
                                            Email = unprocessed.Contact_Email__c,
                                            OwnerId = '00520000001AWKD',
                                            CurrencyIsoCode = unprocessed.CurrencyIsoCode);
            insert newContact;
            mapContactsToInsert.put(newContact.Email,newContact);
        }
    }
    
    global void createAccount(Unprocessed_Agreement__c unprocessed){
        
        if(unprocessed.Customer_ID__c !=''){
            
            Account newAccount=new Account(Name = unprocessed.Customer_Name__c,
                                            Y_tunnus__c= unprocessed.Customer_ID__c,
                                            ShippingStreet= unprocessed.Customer_Address__c,
                                            ShippingPostalCode = unprocessed.Customer_Postal_Code__c,
                                            ShippingCity = unprocessed.AccountShippingCity__c,
                                            Type = 'Client',
                                            Asiakaskategoriat_Luokittelu__c = 'Kehitettävä asiakkuus',
                                            OwnerId = '00520000001AWKD',
                                            RecordTypeId = AccRecType);
            insert newAccount;
            mapAccountsToInsert.put(newAccount.Y_tunnus__c,newAccount);                                                                         
        }               
    }
    
    global void createChatterNotifications(Map<String,Agreement__c> insertedAgreements){
    
        List<Agreement__c> agreements = insertedAgreements.values();
        Map<String,String> mapAgreementsWithAccounts = new Map<String,String>();
        for(Agreement__c agr:agreements){
            mapAgreementsWithAccounts.put(agr.Id,agr.Account__c);
        }
        List<String> accIds = mapAgreementsWithAccounts.values();
        
        Map<String,Account> accountsRelated = new Map<String,Account>([SELECT Name
                                                                       FROM Account
                                                                       WHERE Id IN:accIds]);
        List<FeedItem> feedList = new List<FeedItem>();
        for(Agreement__c a:agreements){
            //REMEMBER TO CHANGE LINK URL AND GROUP FEED ID's WHEN DEPLOYING
            //Production: 0F920000000XZBZ \\ https://eu1.salesforce.com/
            //Sandbox: 0F9M000000006I0 \\ https://cs7.salesforce.com/
            system.debug('----- Account name -----'+accountsRelated.get(a.Account__c));
            FeedItem post = new FeedItem(ParentId = '0F920000000XZBZ',
                                         Body='A new Agreement under '+accountsRelated.get(a.Account__c).Name+' account has been created in Salesforce:\n ',
                                         LinkUrl = 'https://eu1.salesforce.com/'+a.Id,
                                         Title = 'Agreement ID - '+a.Name);
            feedList.add(post);
        }       
        insert feedList;
    }
    
    global void finish(Database.BatchableContext BC) {
    
    }
    
    static testMethod void testAgreementsBatch(){

        test.starttest();
        String q = 'SELECT Id, One_Time_Price__c, Office__c, '+
                    'Night_Price__c, Mobile__c, Extra_Services_Night_Price__c, '+
                    'Ending__c, ISA_Days__c, Customer_Postal_Code__c, AccountShippingCity__c, '+
                    'Customer_Name__c, Customer_ID__c, Customer_Address__c, CurrencyIsoCode, '+
                    'Country_Code__c, Contact_Postal_Office__c, Contact_Name__c, Contact_Email__c, '+
                    'Beginning__c, Apartment_Postal_Code__c, Agreement_ID__c FROM Unprocessed_Agreement__c';        
        AgreementsBatch batch = new AgreementsBatch(q);
        //Preparing dates for Unprocess Agreement record
        Date startDT=Date.newInstance(2012,4,13);       
        Date endDT=Date.newInstance(2012,4,14);
        List<Unprocessed_Agreement__c> listUA = new List<Unprocessed_Agreement__c>();
        Unprocessed_Agreement__c testUA = new Unprocessed_Agreement__c(Agreement_ID__c = '0',
                                                                       Apartment_Postal_Code__c = '00100',
                                                                       Beginning__c = startDT,
                                                                       Contact_Email__c = 'test@test.fi',
                                                                       Contact_Name__c = 'Testi Mies',
                                                                       Country_Code__c = 'FI',
                                                                       Customer_ID__c = '1',
                                                                       Customer_Name__c = 'Test Inc.',
                                                                       Customer_Postal_Code__c = '00101',
                                                                       AccountShippingCity__c = 'Helsinki',
                                                                       Ending__c = endDT+1,
                                                                       Extra_Services_Night_Price__c = 100,
                                                                       ISA_Days__c = 0,
                                                                       Mobile__c = '123456',
                                                                       Night_Price__c = 100,
                                                                       Office__c = 'Kaisaniemi',
                                                                       One_Time_Price__c = 100);
        Unprocessed_Agreement__c updateUA = new Unprocessed_Agreement__c(Agreement_ID__c = '0',
                                                                       Apartment_Postal_Code__c = '11111',
                                                                       Beginning__c = startDT+1,
                                                                       Contact_Email__c = 'bla@bla.fi',
                                                                       Contact_Name__c = 'Second Mies',
                                                                       Country_Code__c = 'SE',
                                                                       Customer_ID__c = '1',
                                                                       Customer_Name__c = 'Test Inc.',
                                                                       Customer_Postal_Code__c = '00101',
                                                                       AccountShippingCity__c = 'Helsinki',
                                                                       Ending__c = endDT,
                                                                       Extra_Services_Night_Price__c = 999,
                                                                       ISA_Days__c = 0,
                                                                       Mobile__c = '123456',
                                                                       Night_Price__c = 99,
                                                                       Office__c = 'Inari',
                                                                       One_Time_Price__c = 900);        
        listUA.add(updateUA);
        insert listUA;
        
        Id batchprocessid = Database.executeBatch(batch, 10);
        
        List<Agreement__c> listAgrs = [SELECT Name
                                       FROM Agreement__c
                                       WHERE Name=:'0'];
   
        List<Agreement__c> updatedAgrs = [SELECT Name
                                          FROM Agreement__c
                                          WHERE Apartment_Postal_Code__c=:'11111'];
        system.assertNotEquals(null,listAgrs);
        system.assertNotEquals(null,updatedAgrs);        
        test.stoptest();
    }
}