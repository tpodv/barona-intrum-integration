public class BaronaExtLoggerTriggerDispatcher {
    
    private Boolean isInsert;
    private Boolean isUpdate;
    private Boolean isBefore;
    private Boolean isAfter;
    
    public BaronaExtLoggerTriggerDispatcher() {
        isInsert = Trigger.isInsert;
        isUpdate = Trigger.isUpdate;
        isBefore = Trigger.isBefore;
        isAfter = Trigger.isAfter;
    }
    
    public void externalLoggerTrigger(List<External_Integration_Logger__c> loggerTrigger, Map<Id, External_Integration_Logger__c> loggerTriggerOld) {
        List<sObject> updateFields = new List<sObject>();
        List<External_Integration_Logger__c> logUpdate = new List<External_Integration_Logger__c>();
        
        for(External_Integration_Logger__c c: loggerTrigger){

            boolean run=false;
            if(loggerTriggerOld!=null) {
                     if(c.sObject_Salesforce_Link__c!=loggerTriggerOld.get(c.Id).sObject_Salesforce_Link__c) run=true;
                     if(c.status__c!=loggerTriggerOld.get(c.Id).status__c && c.status__c!='Error') run = true;
                 }
            if(loggerTriggerOld==null && c.status__c!='Error') run=true;

            if(run) {
                if(isAfter) {
                    Id sf = c.Salesforce_Id__c;
                    sObject upd = sf.getSObjectType().newSObject(c.Salesforce_Id__c);
                    upd.put('A2_Ext_Id__c', c.External_Id__c);
                    upd.put('RunPlatformEvent__c', false);
                    upd.put('Last_Modified_Barona_User__c', c.Barona_User__c);
                    updateFields.add(upd);
                }
                if(isBefore) {
                    c.sObject_Salesforce_Link__c = URL.getSalesforceBaseUrl().toExternalForm()+'/'+c.Salesforce_Id__c;
                    }
            }else{
                system.debug(' externalLoggerTrigger : run is false');
            }
        }
        try {
            if(isAfter && updateFields.size()>0) update updateFields;
        } catch(Exception e){
            for(External_Integration_Logger__c errUp: logUpdate) {
                errUp.status__c='Error';
                errUp.Status_Description__c = 'Error in updating sObject records: '+e.getMessage();
                
            }
        }
    }
}