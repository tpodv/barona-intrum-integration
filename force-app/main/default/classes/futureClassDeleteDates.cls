/*Future class which will be invoked by trigger "createScheduleDates". 
It will delete all the schedule dates which are marked true i.e these are invalid records */

global class futureClassDeleteDates{

    @future 
    public static void deletepastdates() {
        List<Schedule_Date__c> scheduleDates = [Select id,Status_Check__c from Schedule_Date__c where Status_Check__c =:True LIMIT 10000];
        delete scheduleDates;
    }
}