/**
 *
 * TestCreateNewMomentousJobOrder TestClass
 * Created at BiiT Oy
 *
 * - 04/Jan/2012
 * - 29/Feb/2012 - Modifications to fit the new Job Order requirements + code cleanup.
 *
 * This class creates an opportunity of the type 'Momentous' 
 * and updates the opportunity to Won, so the related trigger
 * createNewMomentousJobOrder will fire.
 *
 * To verify that is created checks the number of Job Orders
 * before and after the update.
 *
 * In order to create the test opportunity, the test method
 * creates a test Account and a related Contact record 
 * that will be used in the related Opportunity bearing in mind
 * that both (account and contact) are required fields for the
 * oportunity. 
 *
 * @author Manuel Gonzalez
 *
 */
@isTest
private class testCreateNewMomentousJobOrder {
/*
    static testMethod void createNewMomentousJobOrder() {
        //Record type for the test opportunity
        Id MomentousRecordType = [SELECT Id
                                  FROM RecordType
                                  WHERE Name=:'Momentous' 
                                  AND SOBJECTTYPE=:'Opportunity'].Id;
        
        //Test Account that will be related with the test opportunity.
        Account testAc = new Account (Name = 'Test Account',
                                      Y_tunnus__c = '654321-9',
                                      CurrencyIsoCode = 'EUR',
                                      Type = 'Partner');
        
        insert testAc;
        
        List<Account> relatedTestAcc = [SELECT Id
                                        FROM Account
                                        WHERE Name=:'Test Account'];
        
        //Test Contact related with the account that will be used in the test opportunity.
        //I had to do it this way because the contact has to be related to the Account by requriement.
        Contact testContact = new Contact (Account = relatedTestAcc,
                                           LastName = 'Testing Man',
                                           CurrencyIsoCode = 'EUR',
                                           Email = 'testman@testing.biz');
        
        insert testContact;
        
        //Creation of the test Opportunity
        //Stage='Kontaktointi'
        //Record Type = 'Momentous'
        List<Contact> relatedContact = [SELECT Id
                                        FROM Contact
                                        WHERE Email=:'testman@testing.biz'];
        
        Date closeDateOpp = date.newInstance(1984, 6, 13);
        
        String positionName = 'Test position';
        String oppName = 'Test Opp'; 
        Opportunity testOp = new Opportunity (RecordTypeId = MomentousRecordType,
                                              Name = oppName,
                                              Name_of_the_position__c = positionName,
                                              Toimiala__c = 'Momentous',
                                              WasJobOrderCreated__c = false,
                                              Account = relatedTestAcc[0],
                                              CurrencyIsoCode = 'EUR',
                                              CloseDate = closeDateOpp,
                                              StageName = 'Kontaktointi',
                                              NextStep = 'Testing',
                                              Description = 'Testing');
                                              
        insert testOp;
        
        List<Opportunity> oppToUpdate = [SELECT StageName
                                         FROM Opportunity
                                         WHERE Name=:'Test Opp'];
        
        //List of Job Orders BEFORE the update with the name of the position filled in the opportunity test
        //The size of this list should be 0
        String searchToken = oppName+' - '+positionName;
        List<ts2__Job__c> listOfJobOrdersBefore = [SELECT Id
                                                   FROM ts2__Job__c
                                                   WHERE Name =: searchToken
                                                   LIMIT 1];
                                                                                               
        Test.startTest();
            
            oppToUpdate[0].StageName = 'Voitettu tarjous';
            oppToUpdate[0].Toimiala__c = 'Momentous';
            
            update oppToUpdate;
        
        Test.stopTest();
        
        //List of Job Orders AFTER the update with the name of the position filled in the opportunity test
        //The size of this list should be 1
        List<ts2__Job__c> listOfJobOrdersAfter = [SELECT Id
                                                  FROM ts2__Job__c
                                                  WHERE Name =: searchToken];
        
        //Final check that both Job Order List's size are different 
        //system.assertNotEquals(listOfJobOrdersBefore.size(), listOfJobOrdersAfter.size());
    }
    */
}