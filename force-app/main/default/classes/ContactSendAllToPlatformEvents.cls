/**
 * ContactSendAllToPlatformEvents.cls
 *
 * Batch job to process Accounts.
 * Replace this segment with the respective information.
 *
 * Revision History:
 * 27.02.2019 paul.mcnamara@biit.fi  - Created.
 *
 */

global class ContactSendAllToPlatformEvents implements Database.batchable<sObject> {
  global Database.QueryLocator start(Database.BatchableContext BC){
    return Database.getQueryLocator([
      SELECT   Id, FirstName, A2_Ext_Id__c, AccountId, Contact_Owner_Name__c, OwnerId, Description, CreatedById,
      LastName, Created_By_Name__c, Email, LastModifiedById, Last_Modified_By_Name__c, LastModifiedDate, MobilePhone, Title
       FROM  Contact
    ]);
  }
  
  global void execute(Database.BatchableContext BC, List<Contact> scope){
    processContacts(scope);
  }
  
  global void finish(Database.BatchableContext BC){
    //Send completion email, etc
  }
  
  public static void processContacts(List<Contact> scope) {
    List<Contact_Event__e> contList_ins = new List<Contact_Event__e>(); //List of accounts to be inserted
        
        for(sObject s : scope){
            Contact c = (Contact)s;
            Contact_Event__e contE = new Contact_Event__e();
            
            contE.A2_Ext_Id__c = c.A2_Ext_Id__c;
            contE.Account_Id__c = c.AccountId;
            contE.Account_Name__c = c.AccountId;
            contE.Contact_ID__c = c.Id;
            contE.Contact_Owner__c = c.OwnerId;
            contE.Contact_Owner_Name__c = c.Contact_Owner_Name__c;
            contE.Created_By__c = c.CreatedById;
            contE.Created_By_Name__c = c.Created_By_Name__c;
            contE.Description__c = c.Description;
            contE.Email__c = c.Email;
            contE.First_Name__c = c.FirstName;            
            contE.Last_Modified_By__c = c.LastModifiedById;
            contE.Last_Modified_By_Name__c = c.Last_Modified_By_Name__c;
            contE.Last_Modified_Date__c = c.LastModifiedDate;
            contE.Last_Name__c = c.LastName;
            contE.Mobile__c = c.MobilePhone;
            contE.Operation__c = 'Insert';
            contE.Title__c = c.Title;            
            
            contList_ins.add(contE);
        }   
        
        system.debug('Contacts to be published ' + contList_ins.size());

        // Call method to publish events
        List<Database.SaveResult> results = EventBus.publish(contList_ins);
        // Inspect publishing result for each event
        for (Database.SaveResult sr : results) {
            if (sr.isSuccess()) {
                System.debug('Successfully published contact event.');
            } else {
                for(Database.Error err : sr.getErrors()) {
                    System.debug('Error returned: ' + err.getStatusCode() + ' - ' + err.getMessage());
                }
            }      
        }
  }
}