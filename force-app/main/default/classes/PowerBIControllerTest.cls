@isTest
public class PowerBIControllerTest {	
	
	public static PageReference pageRef = Page.PowerBI_ReportPage;
	public static OAuthApp_pbi__c app;
	public static PowerBIController pbicontroller = new PowerBIController();
	public static PowerBIControllerLtg pbicontrollerLtg = new PowerBIControllerLtg();
		
	@testSetup public static void setUp()
	{	
		app = new OAuthApp_pbi__c();
		app.Name = 'PowerBI';
		app.Token_Expires_On__c = '0';
		app.Client_Id__c = 'clientId';
		app.Client_Secret__c = 'clientSecret';
		app.Authorization_URL__c = 'https://login.windows.net/common/oauth2/authorize';
		app.Access_Token_URL__c = 'https://login.microsoftonline.com/common/oauth2/token';
		app.Resource_URI__c = 'https://analysis.windows.net/powerbi/api';
		insert app;

		pbicontroller.application_name = 'PowerBI';
	}
		
	public static testMethod void powerBiControllerNotNull()
	{
		System.assertNotEquals(pbicontroller, null);
	}
	
	public static testMethod void getValidateResultReturnsNotNull()
	{
		pbicontroller.validateResult = 'testResult';
		String validate = pbicontroller.getValidateResult();
		System.assertEquals('testResult', pbicontroller.getValidateResult());		
	}
	
	public static testMethod void callRedirect()
	{
		PageReference page = pbicontroller.redirectOnCallback();		
	}
	
	public static testMethod void callRefreshToken()
	{	
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
		
		pbicontroller.application_name = 'PowerBI';
		Test.setCurrentPage(pageRef);		

		PageReference page = pbicontroller.refreshAccessToken();
				
		String accessCookie = pbicontroller.PBIAccess_token;
		String refreshCookie =  pbicontroller.PBIRefresh_token;
		
		System.assertEquals('accessCookieToken',accessCookie);
		System.assertEquals('refreshCookieToken',refreshCookie);	
		Test.stopTest();
	}
	
	public static testMethod void ltgRefreshToken() {
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
		
		Test.setCurrentPage(pageRef);		

		String result = PowerBIControllerLtg.refreshAccessToken(null);
		system.debug('result='+result);	
		System.assertEquals(true,result.contains('refresh_token'));	
		Test.stopTest();
		
	}
	public static testMethod void ltgGetAuthUrl() {
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
		
		Test.setCurrentPage(pageRef);		

		String result = PowerBIControllerLtg.getAuthUrl( pageRef.getUrl());
		system.debug('URL result='+result);	
		System.assertEquals(true,result.contains('login'));	
		Test.stopTest();
		
	}
	
	public static testMethod void ltgValidateCodeInApex() {
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
		
		Test.setCurrentPage(pageRef);		

		String result = PowerBIControllerLtg.validateCodeInApex('', pageRef.getUrl());
		system.debug('Validate result='+result);	
		System.assertEquals(true,result.contains('access_token'));	
		Test.stopTest();
		
	}
}