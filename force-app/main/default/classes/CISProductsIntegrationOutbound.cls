public class CISProductsIntegrationOutbound {
  public static Account getAccount(Id accountId){
    Account account = [SELECT Id, Name, Y_tunnus__c FROM Account WHERE Id =: accountId];
    return account;
  }

  @AuraEnabled(cacheable=true)
  public static boolean isCancelAllowed(){
    List<PermissionSetAssignment> count = [SELECT Id FROM PermissionSetAssignment
                                           WHERE AssigneeId = : Userinfo.getUserId() AND PermissionSet.Name = 'CIS_Products_Credit_Rating_Cancellation'];
    boolean isAllowed = count.size() > 0 ? true : false;
    return isAllowed;
  }

  @AuraEnabled(cacheable=true)
  public static string getAccountRatings(Id accountId){
    accountCreditDecision creditDecision = new accountCreditDecision();
    List<CreditDecision__c> creditDecLst =  getAccountRating(accountId);
    system.debug('accountRating='+creditDecLst);
    if( !creditDecLst.isEmpty()) {
      for( CreditDecision__c cr : creditDecLst ) {
        creditDecisionType cdt = getAccountCreditDecisionType(cr);
        if(cr.RecordType.Name == 'Continuous rating' )
          creditDecision.continuousRating = cdt;
        else if(cr.RecordType.Name == 'Continuous credit decision' )
          creditDecision.continuousDecision = cdt;
        else if(cr.RecordType.Name == 'Express credit decision' )
          creditDecision.expressDecision = cdt;
      }
    }
    Credit_limit__c creditLimit = getCreditLimit(accountId);
    if( creditLimit.Id != null ) {
      creditDecision.creditLimit = creditLimit.Amount_of_credit_limit__c;
      creditDecision.creditLimitEndDate = creditLimit.Credit_limit_end_date__c;
      creditDecision.creditLimitStartDate = creditLimit.Credit_limit_start_date__c;
      creditDecision.creditLimitStatus = 'Approved';
    }
    system.debug('creditDecision='+creditDecision);
    return JSON.serialize(creditDecision);
  }

  private static creditDecisionType getAccountCreditDecisionType(CreditDecision__c decision){
    creditDecisionType cdt = new creditDecisionType();
    cdt.assignmentNumber = (Integer)decision.AssignmentNumber__c;
    cdt.id = decision.Id;
    cdt.ratingStatus = decision.CreditRating__c == 'AAA' || decision.CreditRating__c == 'AA' ||
                       decision.CreditRating__c == 'A' || decision.CreditRating__c == 'B' ? 'Granted' : 'Rejected';
    cdt.creditLimit = decision.RecommendedCreditLimit__c;
    cdt.recommendation = decision.CreditRecommendation__c;
    cdt.startDate = decision.StartDate__c;
    return cdt;
  }

  private static List<CreditDecision__c> getAccountRating(Id accountId){
    List<CreditDecision__c> creditDecLst = [SELECT Id, CreditRating__c, CreditRecommendation__c,
                                            RecommendedCreditLimit__c, AssignmentNumber__c,
                                            RecordType.Name, StartDate__c
                                            FROM CreditDecision__c
                                            WHERE Account__c =:accountId AND Status__c ='Open' ORDER BY CreatedDate];
    return creditDecLst;
  }

  private static Credit_limit__c getCreditLimit(Id accountId){
    Credit_limit__c creditLimit = new Credit_limit__c();
    List<Credit_limit__c> creditLimitLst = [SELECT Id, Amount_of_credit_limit__c, Credit_limit_end_date__c,
                                            Credit_limit_status__c, Credit_limit_start_date__c
                                            FROM Credit_limit__c
                                            WHERE Account__c =:accountId AND Credit_limit_status__c = 'Approved'
                                                                                                      AND Credit_limit_start_date__c <= TODAY
                                                                                                      AND (Credit_limit_end_date__c >= TODAY OR Credit_limit_end_date__c = null)
                                                                                                      AND Credit_limit_type__c = 'Credit limit'
                                                                                                                                 ORDER BY Credit_limit_start_date__c
                                                                                                                                 DESC LIMIT 1];
    system.debug('creditLimitLst='+creditLimitLst);
    if( !creditLimitLst.isEmpty() )
      creditLimit = creditLimitLst[0];
    return creditLimit;
  }

  public static HttpResponse getCISProductResponse(string method, string serviceUri, string body ){
    Http http = new Http();
    HttpRequest request = new HttpRequest();
    request.setEndpoint('callout:CIS_Products'+serviceUri);
    request.setHeader('Accept', 'application/json');
    request.setHeader('Accept-Language', 'en-US');
    request.setHeader('Content-Type','application/json');
    request.setTimeout(20000);
    if( method == 'PATCH' ) {
      request.setMethod('PUT');
      request.setHeader('X-HTTP-Method-Override','PATCH');
    }else
      request.setMethod(method);
    request.setBody(body);
    HttpResponse response = http.send(request);
    return response;
  }

  public static calloutResponse getRating(HttpResponse response, string recordTypeName, id accountId, string calloutName){
    calloutResponse result = new calloutResponse();
    Id recordTypeId = Schema.SObjectType.CreditDecision__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
    system.debug(recordTypeName+ ' Response='+response.getBody());
    if( response.getStatusCode() == 200 ) {
      //creditDecision parseResponse = (creditDecision)JSON.deserialize(response.getBody(), creditDecision.class);
      Map<String, Object> responseUntyped = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
      if( responseUntyped != null ) {
        //result = createCreditDecision(accountId, parseResponse, recordTypeId);
        result = createCreditDecision(accountId, calloutName, responseUntyped, recordTypeId);
      }
      system.debug('assignmentNumber='+responseUntyped);
    }else if( response.getStatusCode() == 400 && response.getHeader('CIS-Error-Code') == '713') {
      integer assignmentNumber = integer.valueOf(response.getHeader('CIS-Active-Assignment-Number'));
      if(!Test.IsRunningTest())
        cancelContinuousRating(assignmentNumber);
      result.statusCode = 713;
      result.statusMessage = response.getBody();
    }else{
      result.statusMessage = 'The '+ recordTypeName +' was not updated successfully. Error: '+response.getStatusCode()+', '+response.getStatus();
      result.statusCode = response.getStatusCode();
    }
    return result;
  }

// Continuous rating
  @AuraEnabled
  public static string getContinuousRating(Id accountId){
    string result = '';
    calloutResponse calloutResult = new calloutResponse();
    Account acc = getAccount(accountId);
    string userName = UserInfo.getName();
    string body = '{"companyId": "' + acc.Y_tunnus__c + '","endUserName": "'+userName+'","recipients": ["66140143"],"clientReference": "'+acc.Name+'"}';
    calloutResult = makeCIScallout('Continuous rating', body, 'ContinuousRating', accountId);
    if( calloutResult.statusCode == 713 && !Test.IsRunningTest() )
      result = getContinuousRating(accountId);
    else result = JSON.serialize(calloutResult);
    return result;
  }

// Express Credit Decision
/* @AuraEnabled
   public static string getExpressCreditDecision(Id accountId){
   calloutResponse result = new calloutResponse();
   string userName = UserInfo.getName();
   Account acc = getAccount(accountId);
   string body = ' {"companyId" : "' + acc.Y_tunnus__c + '","endUserName" : "'+userName+'","clientReference" : "' + acc.Name + '"} ';
   result = makeCIScallout('Express credit decision', body, 'ExpressCreditDecision', accountId);

   return JSON.serialize(result);
   }*/

  private static calloutResponse makeCIScallout(string recordTypeName, string body, string calloutService, Id accountId){
    calloutResponse result = new calloutResponse();
    try{
      CIS_Products_Integration_Service__mdt calloutData = getCalloutData(calloutService);
      if( calloutData != null ) {
        HttpResponse response = getCISProductResponse(calloutData.Method_Name__c, calloutData.Resource_name__c, body);
        result = getRating(response, recordTypeName, accountId, calloutData.DeveloperName);
      }else{
        result.statusCode = 100;
        result.statusMessage = 'Integration profile for ' + recordTypeName + ' is not configured. Please contact your system administrator.';
      }
    }catch(System.CalloutException e) {
      result.statusCode = 100;
      result.statusMessage = e.getMessage();
    }
    return result;
  }

  // Continuous Credit Decision Assignment Number
  /* @AuraEnabled
     public static String getContinuousCreditDecisionAN(Id accountId, decimal creditLimit){
     calloutResponse result = new calloutResponse();
     string userName = UserInfo.getName();
     Account acc = getAccount(accountId);
     string body = ' {"companyId" : "' + acc.Y_tunnus__c + '","endUserName" : "'+userName+'",'+
                   ' "requestedCreditLimit" : ' + creditLimit + ',"recipients" : ["66140143"],"clientReference" : "' + acc.Name + '"} ';
     try{
       // Get Assignment number
       CIS_Products_Integration_Service__mdt calloutData = getCalloutData('ContinuousCreditDecision');
       HttpResponse response = getCISProductResponse(calloutData.Method_Name__c, calloutData.Resource_name__c, body);
       if( response.getStatusCode() != 200 ) {
         result.statusCode = response.getStatusCode();
         result.statusMessage = 'Assignment number not applied for customer '+acc.Name +'.Error : '+response.getStatusCode()+', '+response.getStatus();
         return JSON.serialize(result);
       }
       //creditDecision parseResponse = (creditDecision)JSON.deserialize(response.getBody(), creditDecision.class);
       Map<String, Object> parseResponse = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
       Id recordTypeId = Schema.SObjectType.CreditDecision__c.getRecordTypeInfosByName().get('Continuous credit decision').getRecordTypeId();
       // Get Continuous Credit Decision
       if( parseResponse != null ) {
         result = getContinuousCreditDecision((Integer)parseResponse.get('assignmentNumber'),acc);
       }
       else{
         result.statusCode = 100;
         result.statusMessage = 'Unable to get Continuous Credit Decision for customer '+acc.Name + ' Error : Assignment Number not found';
       }
     }catch(Exception e) {
       result.statusCode = 100;
       result.statusMessage = 'Unable to get Continuous Credit Decision for customer '+acc.Name + ' Error : '+e.getMessage();
       system.debug(result);
     }
     return JSON.serialize(result);
     }*/

  // Get Continuous Credit Decision
  //@AuraEnabled
  /* public static calloutResponse getContinuousCreditDecision(integer assignmentNumber, Account acc){
     calloutResponse result = new calloutResponse();
     result = makeCIScallout('Continuous credit decision', '', 'ContinuousRating', acc.Id);
      HttpResponse response = getCISProductResponse ('GET ', 'continuousCreditDecision/' + assignmentNumber, ' ');
        if( response.getStatusCode() != 200 ) {
        result.statusCode = response.getStatusCode();
        result.statusMessage = 'Continuous Credit Decision not found for customer '+acc.Y_tunnus__c +'.Error : '+response.getStatusCode()+', '+response.getStatus();
        return result;
        }
        creditDecision parseResponse = (creditDecision)JSON.deserialize(response.getBody(), creditDecision.class);
        result = getRating(response, 'Continuous credit decision ', acc.Id, 'ContinuousCreditDecision');
     //return result;
     }*/

  public static calloutResponse createCreditDecision(Id accountId, string serviceName,
                                                     Map<String, Object> creditDecisionResponse,
                                                     id recTypeId){
    calloutResponse result = new calloutResponse();
    result.statusMessage = 'Credit Decision record was updated successfully !';
    result.statusCode = 200;
    // Get mapping for custom metadata
    CreditDecision__c creditDecision = new CreditDecision__c();
    List<CIS_Products_Integration_Fields_Mapping__mdt> integrationMappingFields = new List<CIS_Products_Integration_Fields_Mapping__mdt>();
    try{
      integrationMappingFields = [SELECT Id, CIS_Response_field__c, Salesforce_field__c
                                  FROM CIS_Products_Integration_Fields_Mapping__mdt
                                  WHERE CIS_Products_Integration_Service__r.DeveloperName =:serviceName];
      for( CIS_Products_Integration_Fields_Mapping__mdt field :  integrationMappingFields) {
        string responseFieldName = field.CIS_Response_field__c;
        string sfFieldName = field.Salesforce_field__c;
        if( doesFieldExist('CreditDecision__c', sfFieldName) && creditDecisionResponse.containsKey(responseFieldName) ) {
          system.debug('field exists='+field);
          creditDecision.put(sfFieldName, creditDecisionResponse.get(responseFieldName));
        }
        system.debug('creditDecision='+creditDecision);
      }
      if( creditDecision != null ) {
        creditDecision.RecordTypeId = recTypeId;
        creditDecision.Status__c = 'Open';
        creditDecision.Account__c = accountId;
        insert creditDecision;
      }
    }catch(exception e) {
      result.statusMessage = e.getMessage();
      result.statusCode = 100;
      system.debug('CreditDecision__c object cannot be created : '+result);
    }
    return result;
  }

  private static CIS_Products_Integration_Service__mdt getCalloutData(string serviceName){
    CIS_Products_Integration_Service__mdt calloutData = new CIS_Products_Integration_Service__mdt();
    List<CIS_Products_Integration_Service__mdt> calloutDataLst= [SELECT Id, Endpoint_URL__c, Method_Name__c, Resource_name__c, DeveloperName
                                                                 FROM CIS_Products_Integration_Service__mdt
                                                                 WHERE DeveloperName =:serviceName];
    if( !calloutDataLst.isEmpty() )
      calloutData = calloutDataLst[0];
    return calloutData;
  }

  private static boolean doesFieldExist(String objName, string fieldName){
    try {
      SObject so = Schema.getGlobalDescribe().get(objName).newSObject();
      return so.getSobjectType().getDescribe().fields.getMap().containsKey(fieldName);
    }
    catch(Exception ex) {
      system.debug('Error: '+ex.getMessage());
    }

    return false;
  }

  @AuraEnabled
  public static String cancelContinuousRating(integer assignmentNumber){
    calloutResponse result= new calloutResponse();
    string service = '/assignment/' + assignmentNumber + '/status';
    string body = ' {"status" : "CLOSED"} ';
    HttpResponse response = getCISProductResponse('PATCH', service, body);
    system.debug('resetContinuousRating Response='+response.getStatusCode());
    system.debug('cancel Response message='+response.getStatus());
    if( response.getStatusCode() != 200 ) {
      result.statusCode = response.getStatusCode();
      result.statusMessage = 'The Continuous Rating was not cancelled successfully.Error : '+response.getStatusCode()+', '+response.getStatus();
      return JSON.serialize(result);
    }
    result = closeCreditDecision(assignmentNumber);
    return JSON.serialize(result);
  }

  public static calloutResponse closeCreditDecision(integer assignmentNumber){
    calloutResponse result= new calloutResponse();
    result.StatusCode=200;
    result.statusMessage='Credit Decision with assignment number '+assignmentNumber+' was closed successfully.';
    List<CreditDecision__c> creditDecisionLst = [SELECT Id, Status__c
                                                 FROM CreditDecision__c
                                                 WHERE AssignmentNumber__c =: assignmentNumber];
    if( creditDecisionLst.isEmpty() ) {
      result.statusCode=100;
      result.statusMessage='Credit Decision record with assignment number '+assignmentNumber+' was not found in Salesforce.';
      return result;
    }
    try{
      CreditDecision__c creditDecision = creditDecisionLst[0];
      creditDecision.Status__c = 'Closed';
      creditDecision.EndDate__c = (Date)system.today();
      update creditDecision;
    }catch(Exception e) {
      result.statusMessage = 'Error closing CreditDecision with assignment number ' + assignmentNumber + ' : ' + e.getMessage();
      result.statusCode = 100;
      system.debug(result);
    }
    return result;
  }

  private class accountCreditDecision {
    private double creditLimit;
    private date creditLimitEndDate;
    private date creditLimitStartDate;
    private string creditLimitStatus;
    private creditDecisionType continuousRating;
    private creditDecisionType expressDecision;
    private creditDecisionType continuousDecision;
  }

  private class creditDecisionType {
    private integer assignmentNumber;
    private Id id;
    private string ratingStatus;
    private double creditLimit;
    private string recommendation;
    private date startDate;
  }

  private class calloutResponse {
    integer statusCode;
    string statusMessage;
  }
}
