global class ScheduleBatchforIndefiniteAgreement implements Schedulable{
   global void execute(SchedulableContext sc) {
      BatchforIndefiniteAgreement b = new BatchforIndefiniteAgreement(); 
      database.executebatch(b,1);
   }
}