/**
 *
 * TestCreateNewMomentousInvoice TestClass
 * Created at BiiT Oy
 *
 * - 16/Jan/2012 - Created
 * - 19/Jan/2012 - Adapted the previous version to cover 100% code after changed req.
 * - 25/Jan/2012 - Test adapted to new trigger logic. Maintains the structure. Needs optimization (!).
 * - 29/Feb/2012 - Code cleanup. Still needs optimization (!)
 *              
 * This test class selects two Momentous Job Orders to test each of the
 * paths that the new Invoice Trigger can take and cover 100% code.
 * Both select Job Orders already in the system
 * changes the correspondant stage to cover each branch of the trigger (3 or 4 Invoices cycle) 
 * and check if the number of invoices had changed (should be +1).
 * 
 * The key of testing both branches is the field "Second_Invoice_Status_Phase__c"
 *
 * @FIXME: This method needs badly refactoring. There should
 *         be a shorter way to test this.
 * @author Manuel Gonzalez
 *
 */
@isTest
private class testCreateNewMomentousInvoice {
/*
    static testMethod void createNewMomentousInvoice() {
        
        //Building the test structure
        
        Id MomentousJobOrderRecordType = [SELECT Id
                                          FROM RecordType
                                          WHERE Name =: 'Momentous'
                                          AND SOBJECTTYPE=:'ts2__Job__c'].Id;
                                          
        Id MomentousOpportunityRecordType = [SELECT Id
                                             FROM RecordType
                                             WHERE Name =: 'Momentous'
                                             AND SOBJECTTYPE=:'Opportunity'].Id;                              
        
        //Test Account that will be related with the test opportunity.
        Account testAc = new Account (Name = 'Test Account',
                                      Y_tunnus__c = '1234567-8',
                                      CurrencyIsoCode = 'EUR',
                                      Type = 'Partner');
        
        insert testAc;
        
        List<Account> relatedTestAcc = [SELECT Id
                                        FROM Account
                                        WHERE Name=:'Test Account'
                                        LIMIT 1];
        //Creating the opportunity for the Job Orders
        String positionName = 'Test position';
        String oppName = 'Test Opp';
        Date closeDateOpp = date.newInstance(1984, 6, 13); 
        Opportunity testOp = new Opportunity (RecordTypeId = MomentousOpportunityRecordType,
                                              Name = oppName,
                                              Name_of_the_position__c = positionName,
                                              Toimiala__c = 'Momentous',
                                              WasJobOrderCreated__c = false,
                                              Account = relatedTestAcc[0],
                                              CurrencyIsoCode = 'EUR',
                                              CloseDate = closeDateOpp,
                                              StageName = 'Kontaktointi',
                                              NextStep = 'Testing',
                                              Description = 'Testing');
                                              
        insert testOp;
        
        Id oppMockUpId = [SELECT Id
                          FROM Opportunity
                          WHERE Name =: 'Test Opp'
                          LIMIT 1].Id;
                    
        //Selecting a user to to test this. 
        //To create one is far more complicated than to retrieve one.
        Id defaultRecruiter = [SELECT Id 
                               FROM User
                               WHERE UserRoleId IN      
                                                   (SELECT Id
                                                    FROM UserRole 
                                                    WHERE Name=:'Momentous')
                               LIMIT 1].Id;                                

        //Job Order 1
        //Will cover "1 Invoices" cycle 
        ts2__Job__c joMockUp1 = new ts2__Job__c (RecordTypeId = MomentousJobOrderRecordType,
                                                 Name = 'Amulehti - Manager',
                                                 Ammount__c = 1000.0,
                                                 Opportunity__c = oppMockUpId,
                                                 ts2__Account__c = relatedTestAcc[0].Id,
                                                 ts2__Recruiter__c = defaultRecruiter,
                                                 ts2__Status__c = 'Open',
                                                 ts2__Job_Function__c = 'Training',
                                                 ts2__Department__c = 'Professional Service',
                                                 Invoicing_Cycle__c = '1 Invoice',
                                                 Final_Invoice_Status_Phase__c = 'Finalist Presentation');
        insert joMockUp1;
                                                         
        //Job Order 2
        //Will cover "2 Invoices" cycle                                                               
        ts2__Job__c joMockUp2 = new ts2__Job__c (RecordTypeId = MomentousJobOrderRecordType,
                                                 Name = 'Helsingin Sanomat - Photographer',
                                                 Ammount__c = 1000.0,
                                                 Opportunity__c = oppMockUpId,
                                                 ts2__Account__c = relatedTestAcc[0].Id,
                                                 ts2__Recruiter__c = defaultRecruiter,
                                                 ts2__Status__c = 'Open',
                                                 ts2__Job_Function__c = 'Training',
                                                 ts2__Department__c = 'Professional Service',
                                                 Invoicing_Cycle__c = '2 Invoices',
                                                 First_Invoice__c = 'Profile Approved',
                                                 Final_Invoice_Status_Phase__c = 'Finalist Presentation');
        insert joMockUp2;                                                
                                                         
        //Job Order 3
        //Will cover "3 Invoice" cycle
        ts2__Job__c joMockUp3 = new ts2__Job__c (RecordTypeId = MomentousJobOrderRecordType,
                                                 Name = 'BiiT - Software Developer',
                                                 Ammount__c = 1000.0,
                                                 Opportunity__c = oppMockUpId,
                                                 ts2__Account__c = relatedTestAcc[0].Id,
                                                 ts2__Recruiter__c = defaultRecruiter,
                                                 ts2__Status__c = 'Open',
                                                 ts2__Job_Function__c = 'Training',
                                                 ts2__Department__c = 'Professional Service',
                                                 Invoicing_Cycle__c = '3 Invoices',
                                                 Third_Invoice_Status_Phase__c = 'Long List',
                                                 First_Invoice__c = 'Profile Approved',
                                                 Final_Invoice_Status_Phase__c = 'Finalist Presentation');
        insert joMockUp3;                                                
        //Job Order 4                                                                         
        //Will cover "4 Invoices" cycle
        ts2__Job__c joMockUp4 = new ts2__Job__c (RecordTypeId = MomentousJobOrderRecordType,
                                                 Name = 'Barona - Copywriter',
                                                 Ammount__c = 1000.0,
                                                 Opportunity__c = oppMockUpId,
                                                 ts2__Account__c = relatedTestAcc[0].Id,
                                                 ts2__Recruiter__c = defaultRecruiter,
                                                 ts2__Status__c = 'Open',
                                                 ts2__Job_Function__c = 'Training',
                                                 ts2__Department__c = 'Professional Service',
                                                 Invoicing_Cycle__c = '4 Invoices',
                                                 Second_Invoice_Status_Phase__c = 'Profile Approved',
                                                 Third_Invoice_Status_Phase__c = 'Long List',
                                                 First_Invoice__c = 'Profile Approved',
                                                 Final_Invoice_Status_Phase__c = 'Finalist Presentation');
        insert joMockUp4;
        
        List<ts2__Job__c> jobOrder1 = [SELECT Id,
                                              Invoicing_Cycle__c,
                                              Final_Invoice_Status_Phase__c,
                                              ts2__Status__c
                                       FROM ts2__Job__c
                                       WHERE Name =: 'Amulehti - Manager'];
                                       
        List<ts2__Job__c> jobOrder2 = [SELECT Id,
                                              Invoicing_Cycle__c,
                                              Final_Invoice_Status_Phase__c,
                                              First_Invoice__c,
                                              ts2__Status__c
                                       FROM ts2__Job__c
                                       WHERE Name =: 'Helsingin Sanomat - Photographer'];

        List<ts2__Job__c> jobOrder3 = [SELECT Id,
                                              Invoicing_Cycle__c,
                                              Final_Invoice_Status_Phase__c,
                                              First_Invoice__c,
                                              Third_Invoice_Status_Phase__c,
                                              ts2__Status__c
                                       FROM ts2__Job__c
                                       WHERE Name =: 'BiiT - Software Developer'];
                                       
        List<ts2__Job__c> jobOrder4 = [SELECT Id,
                                              Invoicing_Cycle__c,
                                              Final_Invoice_Status_Phase__c,
                                              First_Invoice__c,
                                              Third_Invoice_Status_Phase__c,
                                              Second_Invoice_Status_Phase__c,
                                              ts2__Status__c
                                       FROM ts2__Job__c
                                       WHERE Name =: 'Barona - Copywriter'];                                       
                                                                                                                                                                   
                                                                
        //Job Order 1 Invoices BEFORE the trigger is fired.           
        List<Invoice__c> listInvoices1Before = [SELECT Id 
                                                FROM Invoice__c
                                                WHERE Job_Order__c =: jobOrder1[0].Id];
                                                    
        //Job Order 2 Invoices BEFORE the trigger is fired.                                        
        List<Invoice__c> listInvoices2Before = [SELECT Id 
                                                FROM Invoice__c
                                                WHERE Job_Order__c =: jobOrder2[0].Id];                                                    
        
        //Job Order 3 Invoices BEFORE the trigger is fired.                                        
        List<Invoice__c> listInvoices3Before = [SELECT Id 
                                                FROM Invoice__c
                                                WHERE Job_Order__c =: jobOrder3[0].Id];
        //Job Order 4 Invoices BEFORE the trigger is fired.                                        
        List<Invoice__c> listInvoices4Before = [SELECT Id 
                                                FROM Invoice__c
                                                WHERE Job_Order__c =: jobOrder4[0].Id];                                                    
        
        
        //HERE BEGINGS THE TESTING                                              
        test.startTest();
            
            //Check all the branches
            jobOrder1[0].Invoicing_Cycle__c = '1 Invoice';
            jobOrder1[0].Final_Invoice_Status_Phase__c = 'Completed';
            jobOrder1[0].ts2__Status__c = 'Completed';
            system.debug('@@@@@@@@@@@@@@@@@@@@ '+jobOrder1[0]+ '@@@@@@@@@@@@@');
            update jobOrder1;
            
            jobOrder2[0].Invoicing_Cycle__c = '2 Invoices';
            jobOrder2[0].First_Invoice__c = 'Completed';
            jobOrder2[0].Final_Invoice_Status_Phase__c = 'Completed';
            jobOrder2[0].ts2__Status__c = 'Completed';
            system.debug('################### '+jobOrder2[0]+ '#############');
            update jobOrder2;
                        
            jobOrder3[0].Invoicing_Cycle__c = '3 Invoices';
            jobOrder3[0].First_Invoice__c = 'Completed';
            jobOrder3[0].Third_Invoice_Status_Phase__c = 'Candidate Placed';
            jobOrder3[0].Final_Invoice_Status_Phase__c = 'Completed';
            jobOrder3[0].ts2__Status__c = 'Completed';
            system.debug('%%%%%%%%%%%%%%%%%%%% '+jobOrder3[0]+ '%%%%%%%%%%%%%%%%');
            update jobOrder3;
            
            jobOrder4[0].Invoicing_Cycle__c = '4 Invoices';
            jobOrder4[0].First_Invoice__c = 'Completed';
            jobOrder4[0].Second_Invoice_Status_Phase__c = 'Completed';
            jobOrder4[0].Third_Invoice_Status_Phase__c = 'Candidate Placed';
            jobOrder4[0].Final_Invoice_Status_Phase__c = 'Completed';
            jobOrder4[0].ts2__Status__c = 'Completed';
            system.debug('&&&&&&&&&&&&&&&&&&&&&&&&& '+jobOrder4[0]+ '&&&&&&&&&&&&&&&');
            update jobOrder4;
 
        test.stopTest();
        
        //Job Order 1 Invoices AFTER the trigger is fired.  
        List<Invoice__c> listInvoices1After = [SELECT Id
                                               FROM Invoice__c
                                               WHERE Job_Order__c =: jobOrder1[0].Id];

        //Job Order 2 Invoices AFTER the trigger is fired.                                        
        List<Invoice__c> listInvoices2After = [SELECT Id
                                               FROM Invoice__c
                                               WHERE Job_Order__c =: jobOrder2[0].Id];
        //Job Order 3 Invoices AFTER the trigger is fired.                                        
        List<Invoice__c> listInvoices3After = [SELECT Id
                                               FROM Invoice__c
                                               WHERE Job_Order__c =: jobOrder3[0].Id];
        //Job Order 4 Invoices AFTER the trigger is fired.                                        
        List<Invoice__c> listInvoices4After = [SELECT Id
                                               FROM Invoice__c
                                               WHERE Job_Order__c =: jobOrder4[0].Id];                                                                                                                                                    
        
        //Checking the number of invoices related with that Job Order (should be different)
        //System.assertNotEquals(listInvoices1Before.size(), listInvoices1After.size());
        //System.assertNotEquals(listInvoices2Before.size(), listInvoices2After.size());
        System.assertNotEquals(listInvoices3Before.size(), listInvoices3After.size());
        System.assertNotEquals(listInvoices4Before.size(), listInvoices4After.size());                                                                                            
        
    }
    */
}