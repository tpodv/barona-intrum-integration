/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class testMDMDeletionTriggers {

    static testMethod void testTriggers() {
        Account testAc = new Account (Name = 'Test Account',
                                      Y_tunnus__c = '2112771-3',
                                      CurrencyIsoCode = 'EUR',
                                      Type = 'Organization', iSteerCredit__Credit_Decision__c = 'Granted');
        insert testAc;
        Account savedAc = [SELECT Id FROM Account WHERE Name=:'Test Account'];
        Id testAccId = savedAc.Id;
        
        Toimihenkil__c rekrytoija = new Toimihenkil__c (Etunimi__c = 'Olli', Sukunimi__c = 'Wiren', FinderUsername__c = 'o.wiren', Myyj_tunnus__c = 106580);
        insert rekrytoija;
        Id rekrytoijaId = rekrytoija.Id;
           
        BL_Info__c billingInfo = new BL_Info__c (Tili__c = testAccId, Toimiala__c = 'IT', Rekrytoija__c = rekrytoijaId, sonet_transfer_approved__c = false);
        insert billingInfo;
        Id billingInfoId = billingInfo.Id;
        BL_Info__c savedBillingInfo = [SELECT Id, Tili__c, Toimiala__c, Rekrytoija__c, sonet_transfer_approved__c from BL_Info__c WHERE Id = :billingInfoId];
        
        Opportunity testOpportunity = new Opportunity (Name = 'Test Opportunity', 
                                                    Account = testAc, CloseDate = Date.today() , StageName = 'Kontaktointi');
        insert testOpportunity;
        Opportunity savedOpportunity = [SELECT Id from Opportunity where Id = :testOpportunity.Id];
        
        Contact contact = new Contact(FirstName = 'Test', LastName = 'Contact', Account = testAc);
        insert contact;
        
        Test.startTest();
        
        delete [SELECT Id From BL_Info__c where Id =:billingInfoId];        
        delete [SELECT Id FROM Toimihenkil__c where Id = :rekrytoijaId];
        delete savedOpportunity;
        delete contact;
        delete [SELECT Id FROM Account where Id = : testAccId];
        
        system.assertEquals(5, [ Select count() FROM MDM_Deleted_Object__c ]);
        
        undelete [SELECT Id FROM Account where Id = :testAccId and IsDeleted = true ALL ROWS];
        undelete [SELECT Id FROM Contact where Id = :contact.Id and IsDeleted = true ALL ROWS];
        undelete [SELECT Id FROM Opportunity where Id = :savedOpportunity.Id and IsDeleted = true ALL ROWS];
        undelete [SELECT Id FROM Toimihenkil__c where Id = :rekrytoijaId and IsDeleted = true ALL ROWS];
        // for some reason undeleting this row fails
        //undelete [SELECT Id From BL_Info__c where Id =:billingInfoId and IsDeleted = true ALL ROWS];
        
        system.assertEquals(9, [ Select count() FROM MDM_Deleted_Object__c ]);
        
        system.assertEquals(4, [ Select count() FROM MDM_Deleted_Object__c WHERE undeleted__c = true ]);
        system.assertEquals(5, [ Select count() FROM MDM_Deleted_Object__c WHERE undeleted__c = false ]);        

        Test.stopTest();
    }
}