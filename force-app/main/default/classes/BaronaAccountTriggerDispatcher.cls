public class BaronaAccountTriggerDispatcher {
    
    private Boolean isInsert;
    private Boolean isUpdate;
    private Boolean isBefore;
    private Boolean isAfter;
    private Boolean isUndelete;
    private Boolean deleted;
    
    public BaronaAccountTriggerDispatcher() {
        isInsert = Trigger.isInsert;
        isUpdate = Trigger.isUpdate;
        isBefore = Trigger.isBefore;
        isAfter = Trigger.isAfter;
        isUndelete = Trigger.isUndelete;
        deleted = Trigger.isDelete;
   }

    public void deleteAccountTrigger(List<Account> accountTrigger, List<Account> accountTriggerOld) {

     List<MDM_Deleted_Object__c> deletedObjects = new List<MDM_Deleted_Object__c>();
        List<Account> deletedAccs = new List<Account>();
        
        for(Account c : (isUndelete ? accountTrigger : accountTriggerOld)) {
            deletedObjects.add(new MDM_Deleted_Object__c(Object_Id__c = c.Id, Object_Type__c = 'Account', undeleted__c = Trigger.isUndelete));
            if (!Trigger.isUndelete){
                deletedAccs.add(c);
            }
        }
        
        if (!deletedObjects.isEmpty())
            insert deletedObjects;
        
        if (!deletedAccs.isEmpty())
            PushPlatformEvent.PushAccountEvent(deletedAccs, 'Delete');
    }
    
    public void accountUpdateExt (List<Account> accountTrigger, Map<Id, Account> accountTriggerOld) {
        
        if(isBefore) {
            // Paul Mc Namara  -  adding stages
      /*      List<Account_Stage__c> AccStages = new List<Account_Stage__c>();
                for(Account accd:accountTrigger){
                if(accountTriggerOld==null) accd.RunPlatformEvent__c=true;
                Account_Stage__c accStage = new Account_Stage__c();
                accStage.Name = accd.Name;
                accStage.Description__c = accd.Description;
                accStage.Y_tunnus__c = accd.Y_tunnus__c;
                accStage.Account__c = accd.id;
                AccStages.add(accStage);
            }
            insert AccStages;  */
        }            
        if(isAfter) {
            Boolean publishPlatformE = false;
            system.debug('After event on account and publish event is ' + publishPlatformE);
            Set<Id> runUser = new Set<Id>();
            List<Account_Event__e> acctList_ins = new List<Account_Event__e>(); //List of accounts to be published
            List<External_Integration_Logger__c> extLogList = new List<External_Integration_Logger__c>();
            
            Id usr = BaronaActionExceptionUsers__c.getInstance().FlowUserIgnore__c;
            for(Account accd:accountTrigger){
                if(UserInfo.getProfileId()!=usr && accd.RunPlatformEvent__c==false) {
                   runUser.add(accd.Id);
                }
                if (publishPlatformE){
                    system.debug('Adding platform event');
                    Account_Event__e accE = new Account_Event__e();
                    
                    accE.a2_Ext_Id__c = accd.a2_Ext_Id__c;     
                    accE.Account_Id__c = accd.id;
                    accE.Account_Name__c = accd.Name;
                    accE.Account_Owner__c = accd.OwnerId;
                    accE.Account_owner_Name__c = accd.Account_Owner_Name__c;
                    accE.Account_Record_Type__c = accd.RecordTypeId;
                    accE.Account_Record_Type_Name__c = accd.Account_Record_Type_name__c;
                    accE.Business_number__c = accd.Y_tunnus__c;
                    accE.Contactor_customer_id__c = accd.Contactor_customer_ID__c;
                    accE.Created_By__c = accd.CreatedById;
                    accE.Created_by_Name__c = accd.Created_by_Name__c;
                    accE.Description__c = accd.Description;
                    accE.Last_Modified_By__c = accd.LastModifiedById;
                    accE.Last_Modified_By_Name__c = accd.Last_Modified_By_Name__c;
                    accE.Last_Modified_Date__c = accd.LastModifiedDate;
                    accE.MDM_Id__c = accd.MDM_ID__c;
                    accE.M_files_customer_Id__c = accd.M_files_customer_ID__c;
                    accE.Nav_Approved__c = accd.NAV_approved__c;
                    if (isInsert) accE.Operation__c = 'Insert';
                    if (isUpdate) accE.Operation__c = 'Update';
                    accE.Parent_Account__c = accd.ParentId;
                    accE.Parent_Account_Name__c = accd.Parent_Account_Name__c;
                    accE.Phone__c = accd.Phone;
                    accE.Shipping_City__c = accd.ShippingCity;
                    accE.Shipping_Country__c = accd.ShippingCountry;
                    accE.Shipping_Postal_Code__c = accd.ShippingPostalCode;
                    accE.Shipping_State__c = accd.ShippingState;
                    accE.Street_Address__c = accd.ShippingStreet;
                    
                    
                    acctList_ins.add(accE);
                    
                    External_Integration_Logger__c extlog = new External_Integration_Logger__c();
                    extLog.Salesforce_Id__c = accd.Id;
                    extLog.Status_Description__c = 'BaronaAccountTriggerDispatcher';
                    extLog.sObjectType__c = 'Account';
                    if (isInsert) extLog.Operation__c = 'Insert';
                    if (isUpdate) extLog.Operation__c = 'Update';
                    
                    extLogList.add(extLog);
                }
            }
            
            List<Account> accRun = [Select id, RunPlatformEvent__c from Account where id in:runUser];
            for(Account accUpd:accRun){
                accUpd.RunPlatformEvent__c=true;
                

            }
            if(accRun!=null && accRun.size()>0) update accRun;
            
            System.debug('Number of Accounts found : ' + acctList_ins.size());
            if (publishPlatformE && acctList_ins.size() > 0){
                // Call method to publish events
                List<Database.SaveResult> results = EventBus.publish(acctList_ins);
                // Inspect publishing result for each event
                for (Database.SaveResult sr : results) {
                    if (sr.isSuccess()) {
                        System.debug('Successfully published event.');
                    } else {
                        for(Database.Error err : sr.getErrors()) {
                            System.debug('Error returned: ' + err.getStatusCode() + ' - ' + err.getMessage());
                        }
                    }      
                }
            }
        }
    }
}