/**
 * AccountSendAllToPlatformEvents.cls
 *
 * Batch job to process Accounts.
 * Replace this segment with the respective information.
 *
 * Revision History:
 * 27.02.2019 paul.mcnamara@biit.fi  - Created.
 *
 */

global class AccountSendAllToPlatformEvents implements Database.batchable<sObject> {
    
    global Integer recordsProcessed = 0;
    
  global Database.QueryLocator start(Database.BatchableContext BC){
    return Database.getQueryLocator([
      SELECT   Id, Name, MDM_ID__c, BillingStreet, LastModifiedById,a2_Ext_Id__c, OwnerId, RecordTypeId, Y_tunnus__c, 
      CreatedById,Created_by_Name__c, Account_Record_Type_name__c, Description, Last_Modified_By_Name__c, LastModifiedDate,
        CreatedDate, ParentId, Parent_Account_Name__c, Phone, NAV_approved__c, M_files_customer_ID__c, Contactor_customer_ID__c,
        ShippingStreet, ShippingCity, ShippingCountry, ShippingPostalCode, ShippingState,
       Account_Owner_Name__c FROM  Account
    ]);
  }

  
  global void execute(Database.BatchableContext BC, List<Account> scope){
    processAccounts(scope);
  }
  
  global void finish(Database.BatchableContext BC){
     System.debug('Records processed. Barona!');
        AsyncApexJob job = [SELECT Id, Status, NumberOfErrors,
            JobItemsProcessed,
            TotalJobItems, CreatedBy.Email
            FROM AsyncApexJob
            WHERE Id = :BC.getJobId()];

  }
  
  public static void processAccounts(List<Account> scope) {
    List<Account_Event__e> acctList_ins = new List<Account_Event__e>(); //List of accounts to be published
        
        for(sObject s : scope){
            Account a = (Account)s;
            Account_Event__e accE = new Account_Event__e();
            accE.a2_Ext_Id__c = a.a2_Ext_Id__c;
            accE.Account_Id__c = a.id;
            accE.Account_Name__c = a.Name;
            accE.Account_Owner__c = a.OwnerId;
            accE.Account_owner_Name__c = a.Account_Owner_Name__c;
            accE.Account_Record_Type__c = a.RecordTypeId;
            accE.Account_Record_Type_Name__c = a.Account_Record_Type_name__c;
            accE.Business_number__c = a.Y_tunnus__c;
            accE.Contactor_customer_id__c = a.Contactor_customer_ID__c;
            accE.Created_By__c = a.CreatedById;
            accE.Created_by_Name__c = a.Created_by_Name__c;
            accE.Description__c = a.Description;
            accE.Last_Modified_By__c = a.LastModifiedById;
            accE.Last_Modified_By_Name__c = a.Last_Modified_By_Name__c;
            accE.Last_Modified_Date__c = a.LastModifiedDate;
            accE.MDM_Id__c = a.MDM_ID__c;
            accE.M_files_customer_Id__c = a.M_files_customer_ID__c;
            accE.Nav_Approved__c = a.NAV_approved__c;
            accE.Operation__c = 'Insert';
            accE.Parent_Account__c = a.ParentId;
            accE.Parent_Account_Name__c = a.Parent_Account_Name__c;
            accE.Phone__c = a.Phone;
            accE.Shipping_City__c = a.ShippingCity;
            accE.Shipping_Country__c = a.ShippingCountry;
            accE.Shipping_Postal_Code__c = a.ShippingPostalCode;
            accE.Shipping_State__c = a.ShippingState;
            accE.Street_Address__c = a.ShippingStreet;

            
            acctList_ins.add(accE);
        }      
        
        System.debug('Number of Accounts found : ' + acctList_ins.size());

        // Call method to publish events
        List<Database.SaveResult> results = EventBus.publish(acctList_ins);
        // Inspect publishing result for each event
        for (Database.SaveResult sr : results) {
            if (sr.isSuccess()) {
                System.debug('Successfully published event.');
            } else {
                for(Database.Error err : sr.getErrors()) {
                    System.debug('Error returned: ' + err.getStatusCode() + ' - ' + err.getMessage());
                }
            }      
        }
    }
}