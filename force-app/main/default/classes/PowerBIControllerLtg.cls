public class PowerBIControllerLtg {
	@TestVisible 
	private static String CUSTOM_SETTINGS_APP_NAME = 'PowerBI';
	
	//@TestVisible public String access_token;

	/*
	*  INNER CLASS - The JSON result from a successful oauth call
	*/
	public class OAuthResult {
        /** The access token */
        public String access_token {get; set;}

        /** The refresh token */
        public String refresh_token {get; set;}
        
        /** The token expiry date*/
        public String expires_on {get;set;}
    }
    
    /*
    *   Constructor
    */
    public PowerBIControllerLtg () {
        
    }
    
    
    /** 
    * Gets new access token through refresh token
    *
    * @param refreshToken The refresh token
    * @return The oauth result
    */
    @AuraEnabled
    public static String refreshAccessToken( String parRefreshToken ) {
        //GET setup values from CUSTOM SETTINGS   
        String client_id = OAuthApp_pbi__c.getValues( PowerBIControllerLtg.CUSTOM_SETTINGS_APP_NAME ).Client_Id__c;
        String client_secret = OAuthApp_pbi__c.getValues( PowerBIControllerLtg.CUSTOM_SETTINGS_APP_NAME ).Client_Secret__c;
        String resource_URI = OAuthApp_pbi__c.getValues( PowerBIControllerLtg.CUSTOM_SETTINGS_APP_NAME).Resource_URI__c;
        String access_token_url = OAuthApp_pbi__c.getValues( PowerBIControllerLtg.CUSTOM_SETTINGS_APP_NAME ).Access_Token_URL__c;
        
        String refreshToken = parRefreshToken;
        
        List<String> urlParams = new List<String> {
            'grant_type=refresh_token',
            'refresh_token=' + ( refreshToken == null ? '' : EncodingUtil.urlEncode(refreshToken, 'UTF-8') ),
            'client_id=' + EncodingUtil.urlEncode(client_id, 'UTF-8'),
            'client_secret=' + EncodingUtil.urlEncode(client_secret, 'UTF-8'),
            'resource=' + EncodingUtil.urlEncode(resource_URI, 'UTF-8')
        };
        
        Http h = new Http();

        HttpRequest req = new HttpRequest();
        req.setEndpoint(access_token_url);
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        req.setHeader('Accept', 'application/json');
        String body = String.join(urlParams, '&');
        req.setBody(body);

        HttpResponse res = h.send(req);

        //OAuthResult result = (OAuthResult)(JSON.deserialize(res.getBody(), OAuthResult.class));
        
        //send JSON data back to ltg component    
        String retValue = res.getBody();
        return retValue;
    }
    
    
    /**
    * Gets the authorization URL
    *
    * @return The authorization url
    */
    @AuraEnabled
    public static String getAuthUrl(String parCurrentURL) {
        Map<String, String> urlParams = new Map<String, String> {
            'client_id' => OAuthApp_pbi__c.getValues( PowerBIControllerLtg.CUSTOM_SETTINGS_APP_NAME ).Client_Id__c,
            'redirect_uri' => parCurrentURL,
            'resource' => OAuthApp_pbi__c.getValues( PowerBIControllerLtg.CUSTOM_SETTINGS_APP_NAME ).Resource_URI__c,
            'response_type' => 'code'
        };
        String auth_url = OAuthApp_pbi__c.getValues( PowerBIControllerLtg.CUSTOM_SETTINGS_APP_NAME ).Authorization_URL__c;

        PageReference ref = new PageReference(auth_url);
        ref.getParameters().putAll(urlParams);

        return ref.getUrl();
    }
    
    @AuraEnabled
    public static String validateCodeInApex(String parCode, String parRedirect_uri) {
    	
    	
    	String client_id = OAuthApp_pbi__c.getValues( PowerBIControllerLtg.CUSTOM_SETTINGS_APP_NAME ).Client_Id__c;
        String client_secret = OAuthApp_pbi__c.getValues( PowerBIControllerLtg.CUSTOM_SETTINGS_APP_NAME ).Client_Secret__c;
        String access_token_url = OAuthApp_pbi__c.getValues( PowerBIControllerLtg.CUSTOM_SETTINGS_APP_NAME ).Access_Token_URL__c;
        String resource_URI = OAuthApp_pbi__c.getValues( PowerBIControllerLtg.CUSTOM_SETTINGS_APP_NAME ).Resource_URI__c;

		
        List<String> urlParams = new List<String> {
            'grant_type=authorization_code',
            'code=' + EncodingUtil.urlEncode(parCode, 'UTF-8'),
            'client_id=' + EncodingUtil.urlEncode(client_id, 'UTF-8'),
            'client_secret=' + EncodingUtil.urlEncode(client_secret, 'UTF-8'),
            'redirect_uri=' + EncodingUtil.urlEncode(parRedirect_uri, 'UTF-8')
            //'redirect_uri=' + EncodingUtil.urlEncode(parRedirect_uri, 'UTF-8')
        };


        Http h = new Http();

        HttpRequest req = new HttpRequest();
        req.setEndpoint(access_token_url);
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        req.setHeader('Accept', 'application/json');
        String body = String.join(urlParams, '&');
        req.setBody(body);

        HttpResponse res = h.send(req);
        //validateResult = res.getBody();
        return res.getBody();
    }
          
    /* **********************************************************************************************
    *
    */
    
    
}