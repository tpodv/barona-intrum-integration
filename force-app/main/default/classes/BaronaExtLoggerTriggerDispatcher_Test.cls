/**
 * BaronaExtLoggerTriggerDispatcher_Test
 *
 * Test class for BaronaExtLoggerTriggerDispatcher.
 *
 * Revision History:
 * 19.03.2019 paul.mcnamara@biit.fi  - Created.
 *
 */

@isTest
public class BaronaExtLoggerTriggerDispatcher_Test {

     
    static testmethod void test() { 
        Test.startTest();
        
        Account acc = new Account(Name='Acc 34', BillingStreet='Mannerheimintie 34', MDM_Id__c='MDM0045');
        insert acc;
        system.debug('BaronaExtLoggerTriggerDispatcher_Test - Done insert of Account acc' + acc.Id);
        
        Contact cont = new Contact(Account=acc, Owner=new user(ID = UserInfo.getUserID()), FirstName = 'Cristiano', LastName = 'Ronaldo', Email='ronaldo@barona.fi', MobilePhone='12345678');
        insert cont;
        
        External_Integration_Logger__c logExt = new External_Integration_Logger__c();
        logExt.Salesforce_Id__c = acc.Id;
        insert logExt;
        
        logExt.Operation__c = 'Update';
        update logExt;
        
        List<External_Integration_Logger__c> logUpdate = [SELECT ID, sObject_Salesforce_Link__c FROM External_Integration_Logger__c];
        system.debug('logupdate size is ' + logUpdate.size());
        
        delete acc;
        delete cont;
                
        Test.stopTest();
    }

}