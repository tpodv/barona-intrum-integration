public class PushPlatformEvent {
    
    static public void PushAccountEvent(List<Account> accList, String strOp){    
        List<Account_Event__e> accEvents = new List<Account_Event__e>();
        
        for (Account Acc:accList){
            Account_Event__e accE = new Account_Event__e();
            accE.Operation__c = strOp;
            accE.a2_Ext_Id__c = Acc.a2_Ext_Id__c;
            accE.Account_Id__c = Acc.id;
            accE.Account_Name__c = Acc.Name;
            accE.Account_Owner__c = Acc.OwnerId;
            accE.Account_owner_Name__c = Acc.Account_Owner_Name__c;
            accE.Account_Record_Type__c = Acc.RecordTypeId;
            accE.Account_Record_Type_Name__c = Acc.Account_Record_Type_name__c;
            accE.Business_number__c = Acc.Y_tunnus__c;
            accE.Contactor_customer_id__c = Acc.Contactor_customer_ID__c;
            accE.Created_By__c = Acc.CreatedById;
            accE.Created_by_Name__c = Acc.Created_by_Name__c;
            accE.Description__c = Acc.Description;
            accE.Last_Modified_By__c = Acc.LastModifiedById;
            accE.Last_Modified_By_Name__c = Acc.Last_Modified_By_Name__c;
            accE.Last_Modified_Date__c = Acc.LastModifiedDate;
            accE.MDM_Id__c = Acc.MDM_ID__c;
            accE.M_files_customer_Id__c = Acc.M_files_customer_ID__c;
            accE.Nav_Approved__c = Acc.NAV_approved__c;
            accE.Parent_Account__c = Acc.ParentId;
            accE.Parent_Account_Name__c = Acc.Parent_Account_Name__c;
            accE.Phone__c = Acc.Phone;
            accE.Shipping_City__c = Acc.ShippingCity;
            accE.Shipping_Country__c = Acc.ShippingCountry;
            accE.Shipping_Postal_Code__c = Acc.ShippingPostalCode;
            accE.Shipping_State__c = Acc.ShippingState;
            accE.Street_Address__c = Acc.ShippingStreet;
            accEvents.add(accE);
        }
        
        List<Database.SaveResult> results = EventBus.publish(accEvents);
        
        
        // Inspect publishing result for each event
        for (Database.SaveResult sr : results) {
            if (sr.isSuccess()) {
                System.debug('Successfully published event.');
            } else {
                for(Database.Error err : sr.getErrors()) {
                    System.debug('Error returned: ' + err.getStatusCode() + ' - ' + err.getMessage());
                }
            }      
        }
    }
    
    
    static public void PushContactEvent(List<Contact> conList, String strOp){    
        List<Contact_Event__e> conEvents = new List<Contact_Event__e>();
        
        for (Contact Con:conList){
            Contact_Event__e conE = new Contact_Event__e();
            conE.Contact_ID__c = Con.Id;
            conE.Operation__c = strOp;
            conE.Account_Id__c = Con.AccountId;
            conE.Contact_Owner__c = Con.OwnerId;
            conE.Created_By__c = Con.CreatedById;
            conE.Created_By_Name__c = Con.Created_By_Name__c;
            conE.Description__c = Con.Description;
            conE.Email__c = Con.Email;
            conE.First_Name__c = Con.FirstName;
            conE.Last_Modified_By_Name__c = Con.Last_Modified_By_name__c;
            conE.Last_Modified_Date__c = Con.LastModifiedDate;
            conE.Mobile__c = Con.MobilePhone;
            conE.Title__c = Con.Title;
            conE.A2_Ext_Id__c = Con.A2_Ext_Id__c;
            conEvents.add(conE);
        }
        
        List<Database.SaveResult> results = EventBus.publish(conEvents);
        
        for (Database.SaveResult sr : results) {
            if (sr.isSuccess()) {
                System.debug('Successfully published event.');
            } else {
                for(Database.Error err : sr.getErrors()) {
                    System.debug('Error returned: ' + err.getStatusCode() + ' - ' + err.getMessage());
                }
            }      
        }
        
    }
}