/**
 *
 * TestCreateNewMomentousJobOrder TestClass
 * Created at BiiT Oy
 *
 * - 05/Jan/2012
 *
 *
 * @author Manuel Gonzalez
 *
 */
@isTest
private class testUpdateJobOrderCompleted {
/*
    static testMethod void updateJobOrderCompleted() {
        //Record type for the test opportunity
        Id MomentousOpportunityRecordType = [SELECT Id 
                                             FROM RecordType
                                             WHERE Name=:'Momentous' 
                                             AND SOBJECTTYPE=:'Opportunity'].Id;
        
        //Test Account that will be related with the test opportunity.
        Account testAc = new Account (Name = 'Test Account',
                                      Y_tunnus__c = '1234567-8',
                                      CurrencyIsoCode = 'EUR',
                                      Type = 'Partner');
        insert testAc;
        
        List<Account> relatedTestAcc = [SELECT Id 
                                        FROM Account
                                        WHERE Name=:'Test Account'];
                                        
        //Test Contact related with the account that will be used in the test opportunity.
        //I had to do it this way because the contact has to be related to the Account by requriement.
        Contact testContact = new Contact (Account = relatedTestAcc,
                                           LastName = 'Testing Man',
                                           CurrencyIsoCode = 'EUR',
                                           Email = 'testman@testing.biz');
                                           
        insert testContact;
        
        
        List<Contact> relatedContact = [SELECT Id 
                                        FROM Contact
                                        WHERE Email=:'testman@testing.biz'];
        
        Date closeDateTest = date.newInstance(1984, 6, 13);
        
        //Record Type for the test opportunity related with the job order should be 'Momentous'
        Opportunity testOp = new Opportunity (RecordTypeId = MomentousOpportunityRecordType,
                                              Name = 'Test Opp',
                                              Name_of_the_position__c = 'Testing trigger',
                                              Toimiala__c = 'Momentous',
                                              WasJobOrderCreated__c = false,
                                              Account = relatedTestAcc[0],
                                              Contact__c = relatedContact[0].Id,
                                              CurrencyIsoCode = 'EUR',
                                              CloseDate = closeDateTest,
                                              StageName = 'Kontaktointi',
                                              NextStep = 'Testing more',
                                              Description = 'Fill with chars');
                                              
        //system.debug('@@@@@@@@@@@@@@@@@@@->'+testOp);
        insert testOp;
        
        //relOppBefore will be the related opportunity to the Job Order Test record before update Job Order
        //primaryRecruiter is retrieved to fill the ts2__Recruiter__c mandatory field in Job Order
        //Job_Order_Completed__c in Opportunity needs to be saved to finally compare after update the related Job Order.
        List<Opportunity> relOppBefore = [SELECT StageName,
                                                 Job_Order_Completed__c  
                                          FROM Opportunity 
                                          WHERE Name=:'Test Opp'];
                                          
        Boolean JobOrderCompletedBefore = relOppBefore[0].Job_Order_Completed__c;
        
       
        //Job Order creation
        //Name = 'Job Order Test'
        //ts2__Status__c = 'Open'
        
        Id MomentousJobOrderRecordType = [SELECT Id
                                          FROM RecordType
                                          WHERE Name=:'Momentous' 
                                          AND SOBJECTTYPE=:'ts2__Job__c'].Id;
                                          
        Id primaryRecruiter = [SELECT Id
                               FROM User
                               WHERE Name=:'Intrum Justitia' 
                               LIMIT 1].Id;
        
        ts2__Job__c jobOrderTest = new ts2__Job__c (RecordTypeId = MomentousJobOrderRecordType,
                                                    Name = 'Job Order Test',
                                                    Opportunity__c = relOppBefore[0].Id,
                                                    ts2__Account__c = relatedTestAcc[0].Id,
                                                    ts2__Recruiter__c = primaryRecruiter,
                                                    ts2__Status__c = 'Open',
                                                    ts2__Job_Function__c = 'Training',
                                                    ts2__Department__c = 'Sales');
        
        insert jobOrderTest;
        
        List<ts2__Job__c> listOfJobOrdersToUpdate = [SELECT ts2__Status__c 
                                                     FROM ts2__Job__c
                                                     WHERE Name=:'Job Order Test'];
        
        Test.startTest();
        
            listOfJobOrdersToUpdate[0].ts2__Status__c = 'Completed';
            listOfJobOrdersToUpdate[0].ts2__Date_Filled__c = closeDateTest;
            listOfJobOrdersToUpdate[0].ts2__Closed_Reason__c = 'Won';
            update listOfJobOrdersToUpdate;
        
        Test.stopTest();
        
        List<Opportunity> relOppAfter = [SELECT Job_Order_Completed__c
                                         FROM Opportunity
                                         WHERE Name=:'Test Opp'];
        
        system.assertNotEquals(JobOrderCompletedBefore, relOppAfter[0].Job_Order_Completed__c);
    }
*/
}