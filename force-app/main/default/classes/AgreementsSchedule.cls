global class AgreementsSchedule implements Schedulable{
    
public String query;
    
    global AgreementsSchedule(){
    
        query = 'SELECT Id, '+
                        'One_Time_Price__c, '+
                        'Office__c, '+
                        'Night_Price__c, '+ 
                        'Mobile__c, '+
                        'Extra_Services_Night_Price__c, '+
                        'Ending__c, '+
                        'ISA_Days__c, '+
                        'Customer_Postal_Code__c, '+
                        'AccountShippingCity__c, '+
                        'Customer_Name__c, '+
                        'Customer_ID__c, '+
                        'Customer_Address__c, '+
                        'CurrencyIsoCode, '+
                        'Country_Code__c, '+
                        'Contact_Postal_Office__c, '+
                        'Contact_Name__c, '+
                        'Contact_Email__c, '+ 
                        'Beginning__c, '+
                        'Apartment_Postal_Code__c, '+
                        'Agreement_ID__c '+
                'FROM Unprocessed_Agreement__c';	
    }
    
    global void execute(SchedulableContext SC){
        
        AgreementsSchedule schedule = new AgreementsSchedule();
        AgreementsBatch batch = new AgreementsBatch(schedule.query);
        database.executebatch(batch,10);
    }

	static testMethod void testAgreementSchedule(){
	
		test.starttest();
		String q = 'SELECT Id, One_Time_Price__c, Office__c, '+
					'Night_Price__c, Mobile__c, Extra_Services_Night_Price__c, '+
					'Ending__c, ISA_Days__c, Customer_Postal_Code__c, AccountShippingCity__c, '+
					'Customer_Name__c, Customer_ID__c, Customer_Address__c, CurrencyIsoCode, '+
					'Country_Code__c, Contact_Postal_Office__c, Contact_Name__c, Contact_Email__c, '+
					'Beginning__c, Apartment_Postal_Code__c, Agreement_ID__c FROM Unprocessed_Agreement__c';
		
		AgreementsSchedule t = new AgreementsSchedule();
        t.execute(null);
		system.assertEquals(t.query, q);
        

        test.stoptest();
	}
}