/**
 * AccountSendAllToPlatformEvents_Test
 *
 * Test class for AccountSendAllToPlatformEvents.
 *
 * Revision History:
 * 01.03.2019 paul.mcnamara@biit.fi  - Created.
 *
 */

@isTest
public class ContactSendAllToPlatformEvents_Test {
 @testSetup
    static void setup() {
        // Create data for the test
        List<Account> accs = new List<Account>();
        List<Contact> conts = new List<Contact>();
        // insert 200 accounts
        for (Integer i=0;i<200;i++) {
            accs.add(new Account(Name='Acc '+i,
                BillingStreet='Mannerheimintie '+i, MDM_Id__c='MDM00'+i));
        }
        insert accs;
        // insert 200 contacts
        for (Account acc :accs){
            conts.add(new Contact(AccountId=acc.id, FirstName='Cont '+acc.Name, LastName = 'Last'+acc.Name));
        }
        insert conts;
    }
 
    static testmethod void test() { 
        Test.startTest();
        ContactSendAllToPlatformEvents ac = new ContactSendAllToPlatformEvents();
        Id batchId = Database.executeBatch(ac);
        Test.stopTest();
    }
}