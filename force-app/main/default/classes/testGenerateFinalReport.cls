/**
 *
 * testGenerateFinalReport Test Class
 * Created at BiiT Oy
 *
 * - 01/Mar/2012 - Created
 *
 *
 * @author Manuel Gonzalez
 *
 */
@isTest
private class testGenerateFinalReport {
/*
    static testMethod void testAllChartGenerationMethods() {
        
       PageReference pageRef = Page.getFinalReport;
       
       Test.setCurrentPageReference(pageRef); 
       //Add parameters to page URL
       
        Id MomentousRecordType = [SELECT Id
                                  FROM RecordType
                                  WHERE Name=:'Momentous' 
                                  AND SOBJECTTYPE=:'Opportunity'].Id;
        
        Account testAc = new Account (Name = 'Test Account',
                                      Y_tunnus__c = '1234567-8',
                                      CurrencyIsoCode = 'EUR',
                                      Type = 'Partner');
        insert testAc;
        
        List<Account> relatedTestAcc = [SELECT Id 
                                        FROM Account
                                        WHERE Name=:'Test Account'];                                      
                                  
        Contact testContact = new Contact (Account = relatedTestAcc,
                                           LastName = 'Testing Man',
                                           CurrencyIsoCode = 'EUR',
                                           Email = 'testman@testing.biz');
        insert testContact;
        
        List<Contact> relatedContact = [SELECT Id
                                        FROM Contact
                                        WHERE Email=:'testman@testing.biz'];

        Date closeDateOpp = date.newInstance(1984, 6, 13);                                                                                         
                                                                                 
        Opportunity testOp = new Opportunity (RecordTypeId = MomentousRecordType,
                                              Name = 'Test Opp',
                                              Name_of_the_position__c = 'Testing trigger',
                                              Toimiala__c = 'Momentous',
                                              WasJobOrderCreated__c = false,
                                              Account = relatedTestAcc[0],
                                              Contact__c = relatedContact[0].Id,
                                              CurrencyIsoCode = 'EUR',
                                              CloseDate = closeDateOpp,
                                              StageName = 'Kontaktointi',
                                              NextStep = 'Testing more',
                                              Description = 'Fill with chars');
        insert testOp;
        
        List<Opportunity> oppMockUp = [SELECT StageName
                                         FROM Opportunity
                                         WHERE Name=:'Test Opp'];       
                                                                                             
        Id MomentousJobOrderRecordType = [SELECT Id
                                          FROM RecordType
                                          WHERE Name=:'Momentous' 
                                          AND SOBJECTTYPE=:'ts2__Job__c'].Id;
        Id primaryRecruiter = [SELECT Id
                               FROM User
                               WHERE Name=:'Intrum Justitia' 
                               LIMIT 1].Id;                                                                              
       
       ts2__Job__c jobOrderMockUp = new ts2__Job__c (RecordTypeId = MomentousJobOrderRecordType,
                                                    Name = 'Job Order Test',
                                                    Opportunity__c = oppMockUp[0].Id,
                                                    ts2__Account__c = relatedTestAcc[0].Id,
                                                    ts2__Recruiter__c = primaryRecruiter,
                                                    ts2__Status__c = 'Open',
                                                    ts2__Job_Function__c = 'Training',
                                                    ts2__Department__c = 'Sales',
                                                    Property_1__c = 'Action', 
                                                    Property_2__c = 'Achieving', 
                                                    Property_3__c = 'Competitive',
                                                    Property_4__c = 'Fear of failure',
                                                    Property_5__c = 'Immersed to work',
                                                    Property_6__c = 'Commercialism',
                                                    Property_7__c = 'Authority',
                                                    Property_8__c = 'Connection to others',
                                                    Property_9__c = 'Recognition',
                                                    Property_10__c = 'Interest',
                                                    Value_1__c = 2 ,
                                                    Value_2__c = 2,
                                                    Value_3__c = 2,
                                                    Value_4__c = 2,
                                                    Value_5__c = 2,
                                                    Value_6__c = 2,
                                                    Value_7__c = 2,
                                                    Value_8__c = 2,
                                                    Value_9__c = 2,
                                                    Value_10__c = 2,
                                                    Personality_1__c = 'Deciding and initiating action',
                                                    Personality_2__c = 'Leading and supervising',
                                                    Personality_3__c = 'Working with people',
                                                    Personality_4__c = 'Analysing',
                                                    Personality_5__c = 'Writing and reporting',
                                                    Personality_6__c = 'Relating and networking',
                                                    Personality_7__c = 'Planning and organising',
                                                    Personality_8__c = 'Learning and researching',
                                                    Personality_9__c = 'Achieving personal work goals',
                                                    Personality_10__c = 'Formulating strategies and concepts',
                                                    Value_Personality_1__c = 2, 
                                                    Value_Personality_2__c = 2,
                                                    Value_Personality_3__c = 2,
                                                    Value_Personality_4__c = 2,
                                                    Value_Personality_5__c = 2,
                                                    Value_Personality_6__c = 2,
                                                    Value_Personality_7__c = 2,
                                                    Value_Personality_8__c = 2,
                                                    Value_Personality_9__c = 2,
                                                    Value_Personality_10__c = 2,
                                                    Skill_Level_Target_1__c = 2,
                                                    Skill_Level_Target_2__c = 2,
                                                    Skill_Level_Target_3__c = 2,
                                                    Skill_Level_Target_4__c = 2,
                                                    Skill_Level_Target_5__c = 2,
                                                    Skill_Target_1__c = 'A',
                                                    Skill_Target_2__c = 'B',
                                                    Skill_Target_3__c = 'C',
                                                    Skill_Target_4__c = 'D',
                                                    Skill_Target_5__c = 'E',
                                                    Process_vs_Results_Target__c = 2,
                                                    Parochial_vs_Professional_Target__c = 2,
                                                    Loose_control_vs_Tight_control_Target__c = 2,
                                                    Employee_vs_Job_Target__c = 2,
                                                    Open_system_vs_Closed_system_Target__c = 2,
                                                    Normative_vs_Pragmatic_Target__c = 2);
        insert jobOrderMockUp;
        
        List<ts2__Job__c> jobOrder = [SELECT Id
                                      FROM ts2__Job__c
                                      WHERE Name=:'Job Order Test'
                                      LIMIT 1];                                                                                      
        
       //system.debug('@@@@@@@@@@@@@@@@@@@@'+jobOrder);
                                             
       ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(jobOrder[0]);
       ApexPages.currentPage().getParameters().put('Id', jobOrder[0].Id);
       

       //system.debug('######################'+sc);
       generateFinalReport testCon = new generateFinalReport(sc);
       
       string val1 = testCon.getChart1();
       string val2 = testCon.getChart2();
       string val3 = testCon.getChart3();
       string val4 = testCon.getChart4();
       
       system.assertNotEquals(val1, null);
       system.assertNotEquals(val2, null);
       system.assertNotEquals(val3, null);
       system.assertNotEquals(val4, null);
       
    }
    */
}