/* Batch to create Schedule Date for Indefinite Agreement*/

global class BatchforIndefiniteAgreement implements Database.Batchable<sObject>,Database.stateful{
    global String ret = '';
    //global Integer successNum = 0 , errorNum = 0;
    global Set<Id> successIds = new Set<ID>();
    global SEt<Id> errorIds = new SEt<ID>();
                         
                         
    global Database.querylocator start(Database.BatchableContext BC){
          String query='select id,Type__c,Beginning__c,Extra_Services_Night_Price__c,CurrencyIsoCode,Night_Price__c,Schedule_Duration__c from Agreement__c where Type__c=\'Indefinite agreement\'';
          return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope){
        Map<Id,Agreement__c> AgreeMap = new Map<ID,Agreement__c>();
        List<id> Agreementlist = new List<id>();
        list<Schedule_Date__c> schedulelist = new List<Schedule_Date__c>();
        List<Id> ScheduleDateDeletelist = new List<Id>();  
        List<Schedule_Date__c> ScheduleDateList = new List<Schedule_Date__c>(); 
      
       for(sObject s : scope){
           Agreement__c Agreement= (Agreement__c)s;
           
           if(Agreement.Type__c=='Indefinite agreement'){
               Agreementlist.add(Agreement.id);              
               AgreeMap.put(Agreement.id,Agreement);
           }
       }
       
       // get all previous schedule and delete them
       schedulelist = [select id from Schedule_Date__c where Accommodation_Agreement__c IN: Agreementlist];
        
   
       if(schedulelist.size()>0)
           delete schedulelist;
   
       if(Agreementlist.size() > 0){ 
         
           //Iterate through Agreements to create schedule dates    
           for(Id aId: Agreementlist){
           system.debug('check is');
               Agreement__c Agr  = AgreeMap.get(aId);
               date dt = agr.Beginning__c;
               
               if(dt >= agr.Schedule_Duration__c)
                   errorIds.add(aID);
                   
               //Create schedule date records equal to number of days between beginning and ending dates. 
               while(dt < agr.Schedule_Duration__c){
                   if((agr.Extra_Services_Night_Price__c!=null)||(agr.Night_Price__c!=null)){
                       Schedule_Date__c SchDateService = new Schedule_Date__c();
                       
                       if(agr.Extra_Services_Night_Price__c!=null)
                           SchDateService.Extra_Service_Amount__c = agr.Extra_Services_Night_Price__c;
                       if(agr.Night_Price__c!=null)
                           SchDateService.Daily_Amount__c = agr.Night_Price__c;
                           SchDateService.Accommodation_Agreement__c = agr.ID;
                           SchDateService.Date__c = dt;
                           SchDateService.CurrencyIsoCode=agr.CurrencyIsoCode;
                           ScheduleDateList.add(SchDateService);   
                       }
                        
                       dt = dt.adddays(1);
                 }
             } 
              
           
                   
               if(ScheduleDateList.size()>0){
                   List<Database.SaveResult> uResults;
                   
                   try{
                       uResults = Database.insert(ScheduleDateList);
                   
                   }
                   catch(Exception e){ ret = 'There was an error when upserting the Records';  ret += e;  
                   }
                   
                    //Logic for sending Status Email
                    Integer i = 0,j=0,k=0;
                    String body = '';
                    
                    Database.Error err;
                    list<integer> rowNo = new list<integer>();
                   
                    for(Database.SaveResult resultError:uResults) {
                        i++;
                        if (resultError.isSuccess()){
                            j++;
                            successIds.add(ScheduleDateList[i-1].Accommodation_Agreement__c);
                            continue;
                        }
                        else {
                            k++;
                            errorIds.add(ScheduleDateList[i-1].Accommodation_Agreement__c);
                            err = resultError.getErrors()[0];
                            
                            //body += 'Row:'+i +'  Customer Code: '+actualList[i-1].Customer__c+' 
                            //body += 'Record No:' +i +' Exception: '+err.getMessage()+'\n';
                        }
                    }
                    
                  /*  if(i != j ){
                        errorNum  += k;
                        successNum += j;
                        
                        system.debug('Error '+errorNUm +'   '+successnum);
                    }
                    else{
                        successNum += j;
                        system.debug('Success '+successNum);
                    }
                    */
              } 
              
                     
       }
    }
        
    global void finish(Database.BatchableContext BC){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        //customer.service@biit.fi.
        mail.setToAddresses(new String[] {'customer.service@biit.fi'});
        
        mail.setSubject('Indefinite Agreement Batch Status Report');
        
        String body = 'Success: ' +successIds.size()+' agreement records are processed successfully.\n'+
                              'Failure: '+ errorIds.size()+' agreement records failed to be processed';
        if(ret != '')
            body = ret;
            
        mail.setPlainTextBody(body);
        
        
        //Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}