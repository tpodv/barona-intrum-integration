@isTest
public class CISProductsIntegrationOutboundTest {
  @testSetup
  static void setup() {
    Account acc = getAccount('1904292-1', 'IntegrationTestAccount');
    insert acc;
    date startDate = system.today();
    date endDate = startdate.addMonths(10);
    Credit_limit__c creditLimit = getCreditLimit(acc.Id, startDate, EndDate);
    insert creditLimit;
  }

  static testMethod void CreateContinuousRating(){
    StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
    Id accId = [SELECT Id FROM Account WHERE Y_tunnus__c = '1904292-1'].Id;
    mock.setStaticResource('CISProducts_ContinuousRatingResponse');
    mock.setStatusCode(200);
    mock.setHeader('Content-Type', 'application/json');
    Test.setMock(HttpCalloutMock.class, mock);
    Test.startTest();
    string result = CISProductsIntegrationOutbound.getContinuousRating(accId);
    string ratingResult = CISProductsIntegrationOutbound.getAccountRatings(accId);
    List<CreditDecision__c> creditDecLst = [SELECT Id, CreditRating__c, CreditRecommendation__c,
                                            RecommendedCreditLimit__c, AssignmentNumber__c
                                            FROM CreditDecision__c
                                            WHERE Account__c =:accId AND Status__c ='Open' ORDER BY CreatedDate];
    system.assertEquals(creditDecLst.size(), 1);
    system.assertEquals(creditDecLst[0].AssignmentNumber__c, 2082073);
    Test.stopTest();
  }

  static testMethod void DuplicateContinuousRating(){
    StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
    Id accId = [SELECT Id FROM Account WHERE Y_tunnus__c = '1904292-1'].Id;
    mock.setStaticResource('CISProducts_ContinuousRatingResponse');
    mock.setStatusCode(400);
    mock.setHeader('Content-Type', 'application/json');
    mock.setHeader('CIS-Error-Code', '713');
    mock.setHeader('CIS-Active-Assignment-Number', '12345');
    Test.setMock(HttpCalloutMock.class, mock);
    Test.startTest();
    string result = CISProductsIntegrationOutbound.getContinuousRating(accId);
    Test.stopTest();
  }


  static testMethod void CancelContinuousRating(){
    Id accId = [SELECT Id FROM Account WHERE Y_tunnus__c = '1904292-1'].Id;
    integer assignmentNumber = 12345;
    CreditDecision__c creditDecision = getCreditDecision(accId, assignmentNumber, 'Continuous rating');
    StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
    mock.setStaticResource('CISProducts_ContinuousRatingResponse');
    mock.setStatusCode(200);
    mock.setHeader('Content-Type', 'application/json');
    insert creditDecision;
    Test.setMock(HttpCalloutMock.class, mock);
    Test.startTest();
    string result = CISProductsIntegrationOutbound.cancelContinuousRating(assignmentNumber);
    Test.stopTest();
    List<CreditDecision__c> creditDecLst = [SELECT Id, CreditRating__c, CreditRecommendation__c,
                                            RecommendedCreditLimit__c, AssignmentNumber__c, EndDate__c
                                            FROM CreditDecision__c
                                            WHERE Account__c =:accId AND Status__c ='Closed' ORDER BY CreatedDate];
    system.assertEquals(creditDecLst.size(), 1);
    system.assertEquals(creditDecLst[0].EndDate__c, System.today());
  }

  static testMethod void CancelContinuousRatingFailed(){
    Id accId = [SELECT Id FROM Account WHERE Y_tunnus__c = '1904292-1'].Id;
    integer assignmentNumber = 12345;
    StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
    mock.setStaticResource('CISProducts_ContinuousRatingResponse');
    mock.setStatusCode(300);
    mock.setHeader('Content-Type', 'application/json');
    Test.setMock(HttpCalloutMock.class, mock);
    Test.startTest();
    string result = CISProductsIntegrationOutbound.cancelContinuousRating(assignmentNumber);
    Test.stopTest();
    List<CreditDecision__c> creditDecLst = [SELECT Id, CreditRating__c, CreditRecommendation__c,
                                            RecommendedCreditLimit__c, AssignmentNumber__c, EndDate__c
                                            FROM CreditDecision__c
                                            WHERE Account__c =:accId AND Status__c ='Closed' ORDER BY CreatedDate];
    system.assertEquals(creditDecLst.size(), 0);
  }

  static testMethod void CheckCancellation(){
    Test.startTest();
    boolean isCancelAllowed = CISProductsIntegrationOutbound.isCancelAllowed();
    Test.stopTest();
    system.assertEquals(isCancelAllowed, true);
  }


  private static Account getAccount(string businessId, string name){
    id recTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Organization').getRecordTypeId();
    Account acc = new Account();
    acc.Name = name;
    acc.Y_tunnus__c = businessId;
    acc.RecordTypeId = recTypeId;
    return acc;
  }

  private static Credit_limit__c getCreditLimit(id accountId, date startDate, date EndDate){
    Credit_limit__c creditLimit = new Credit_limit__c();
    creditLimit.Credit_limit_type__c = 'Credit limit';
    creditLimit.Credit_limit_status__c = 'Approved';
    creditLimit.Amount_of_credit_limit__c = 40000;
    creditLimit.Credit_limit_start_date__c = startDate;
    creditLimit.Credit_limit_end_date__c = endDate;
    creditLimit.Account__c = accountId;
    return creditLimit;
  }

  private static CreditDecision__c getCreditDecision(Id accountId, integer assignmentNumber, string recordTypeName){
    CreditDecision__c creditDecision = new CreditDecision__c();
    creditDecision.Account__c = accountId;
    creditDecision.AssignmentNumber__c = assignmentNumber;
    creditDecision.Status__c ='Open';
    creditDecision.RecordTypeId = Schema.SObjectType.CreditDecision__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
    return creditDecision;
  }
}