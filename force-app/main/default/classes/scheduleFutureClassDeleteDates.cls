global class scheduleFutureClassDeleteDates implements Schedulable{
    
    global scheduleFutureClassDeleteDates(){}
    
    global void execute(SchedulableContext sc){
    
    	futureClassDeleteDates.deletepastdates();
              
    }
    
    static testMethod void testScheduleFutureClassDeleteDates(){
    	
        test.startTest();
        scheduleFutureClassDeleteDates testing = new scheduleFutureClassDeleteDates();
        testing.execute(null);
        test.stopTest();
    }

}