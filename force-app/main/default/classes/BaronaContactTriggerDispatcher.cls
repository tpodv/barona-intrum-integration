public class BaronaContactTriggerDispatcher {
    
    private Boolean isInsert;
    private Boolean isUpdate;
    private Boolean isBefore;
    private Boolean isAfter;
    private Boolean isUndelete;
    private Boolean deleted;
    
    public BaronaContactTriggerDispatcher() {
        isInsert = Trigger.isInsert;
        isUpdate = Trigger.isUpdate;
        isBefore = Trigger.isBefore;
        isAfter = Trigger.isAfter;
        isUndelete = Trigger.isUndelete;
        deleted = Trigger.isDelete;
    }
    
    public void deleteContactTrigger(List<Contact> triggerContact, List<Contact> triggerContactOld) {
        
        List<MDM_Deleted_Object__c> deletedObjects = new List<MDM_Deleted_Object__c>();
        List<Contact> deletedCons = new List<Contact>();
        
        for(Contact c : (isUndelete ? triggerContact : triggerContactOld)) {
            deletedObjects.add(new MDM_Deleted_Object__c(Object_Id__c = c.Id, Object_Type__c = 'Contact', undeleted__c = Trigger.isUndelete));
            if (!Trigger.isUndelete){
                deletedCons.add(c);
            }
            
        }
        
        if (!deletedObjects.isEmpty())
            insert deletedObjects;
        
        if (!deletedCons.isEmpty())
            PushPlatformEvent.PushContactEvent(deletedCons, 'Delete');  
        
    }
    
    public void contactUpdateExt (List<Contact> contactTrigger, Map<Id, Contact> contactTriggerOld) {
        
        if(isBefore) {
            for(Contact cont:ContactTrigger){
                if(ContactTriggerOld==null) cont.RunPlatformEvent__c=true;
            }
        }            
        if(isAfter) {
            Boolean publishPlatformE = false;
            Set<Id> runUser = new Set<Id>();
            List<Contact_Event__e> contList_ins = new List<Contact_Event__e>();
            List<External_Integration_Logger__c> extLogList = new List<External_Integration_Logger__c>();
            Id usr = BaronaActionExceptionUsers__c.getInstance().FlowUserIgnore__c;
            for(Contact cont:ContactTrigger){
                if(UserInfo.getProfileId()!=usr && cont.RunPlatformEvent__c==false) {
                    runUser.add(cont.Id);
                }
                
                if (publishPlatformE){
                    Contact_Event__e contE = new Contact_Event__e();
                    
                    contE.a2_Ext_Id__c = cont.A2_Ext_Id__c;
                    contE.Account_Id__c = cont.AccountId;
                    contE.Account_Name__c = cont.AccountId;
                    contE.Contact_ID__c = cont.Id;
                    contE.Contact_Owner__c = cont.OwnerId;
                    contE.Contact_Owner_Name__c = cont.Contact_Owner_Name__c;
                    contE.Created_By__c = cont.CreatedById;
                    contE.Created_By_Name__c = cont.Created_By_Name__c;
                    contE.Description__c = cont.Description;
                    contE.Email__c = cont.Email;
                    contE.First_Name__c = cont.FirstName;            
                    contE.Last_Modified_By__c = cont.LastModifiedById;
                    contE.Last_Modified_By_Name__c = cont.Last_Modified_By_Name__c;
                    contE.Last_Modified_Date__c = cont.LastModifiedDate;
                    contE.Last_Name__c = cont.LastName;
                    contE.Mobile__c = cont.MobilePhone;
                    if (isInsert) contE.Operation__c = 'Insert';
                    if (isUpdate) contE.Operation__c = 'Update';
                    contE.Title__c = cont.Title;            
                    
                    contList_ins.add(contE);               
                    
                    
                    External_Integration_Logger__c extlog = new External_Integration_Logger__c();
                    extLog.Salesforce_Id__c = cont.Id;
                    extLog.Status_Description__c = 'BaronaContactTriggerDispatcher';
                    extLog.sObjectType__c = 'Contact';
                    if (isInsert) extLog.Operation__c = 'Insert';
                    if (isUpdate) extLog.Operation__c = 'Update';
                    
                    extLogList.add(extLog);
                }
            }
            
            List<Contact> contRun = [Select id, RunPlatformEvent__c from Contact where id in:runUser];
            for(Contact accUpd:contRun){
                accUpd.RunPlatformEvent__c=true;
            }
            if(contRun!=null && contRun.size()>0) update contRun;
            
            System.debug('Number of Contacts found : ' + contList_ins.size());
            if (publishPlatformE && contList_ins.size() > 0){
                system.debug('Contacts to be published ' + contList_ins.size());
                
                
                List<Database.SaveResult> results = EventBus.publish(contList_ins);
                // Inspect publishing result for each event
                for (Database.SaveResult sr : results) {
                    if (sr.isSuccess()) {
                        System.debug('Successfully published contact event.');
                    } else {
                        for(Database.Error err : sr.getErrors()) {
                            System.debug('Error returned: ' + err.getStatusCode() + ' - ' + err.getMessage());
                        }
                    }      
                }
            }
        }
    }
    
}