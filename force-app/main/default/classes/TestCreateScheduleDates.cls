/* Test Class for CreateScheduleDates Trigger*/

@isTest
private class TestCreateScheduleDates{   
   
     static testmethod void testCreateScheduleDates() {
         
         
          Account testAccount = new Account(Name='test', Y_tunnus__c = '1234567-9');
         insert testAccount;
         
         Agreement__c agree = new Agreement__c();
         agree.name='Test1';
         agree.Night_Price__c= 100;
         agree.Type__c= 'Fixed term agreement';
         agree.Beginning__c= system.today();
         agree.Ending__c= system.today().addDays(1);
         agree.Extra_Services_Night_Price__c= 200;
         agree.Account__c = testAccount.Id;
     
    
         Test.startTest();
         
         insert agree;
         agree.Night_Price__c= 1000;
         agree.Type__c= 'Fixed term agreement';
         agree.Beginning__c= system.today();
         agree.Ending__c= system.today().addDays(1);
         agree.Extra_Services_Night_Price__c= 2000;
         update agree;
     
         Test.StopTest();
     }  
}