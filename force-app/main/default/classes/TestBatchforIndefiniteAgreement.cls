@isTest
private class TestBatchforIndefiniteAgreement{   

    static testmethod void testBatchforIndefiniteAgreement(){
        
         Account testAccount = new Account(Name='test', Y_tunnus__c = '1234567-9');
         insert testAccount;
         
         Agreement__c agree = new Agreement__c();
         agree.name='Test';
         agree.Night_Price__c= 100;
         agree.Type__c= 'Indefinite agreement';
         agree.Beginning__c= system.today();
         agree.CurrencyIsoCode= 'EUR';
         agree.Extra_Services_Night_Price__c= 200;
         agree.ISA_Days__c = 2;
         agree.Account__c = testAccount.Id;
         insert agree;
         
         Schedule_Date__c sch = new Schedule_Date__c();
         //sch.name='xyz';
         sch.Accommodation_Agreement__c= agree.id;
         insert sch;
         
         list<Agreement__c> agreement=[select id,name,ISA_Days__c,Night_Price__c,Type__c,CurrencyIsoCode,Beginning__c,Schedule_Duration__c,Extra_Services_Night_Price__c from Agreement__c where Type__c='Indefinite agreement'];
         BatchforIndefiniteAgreement c= new BatchforIndefiniteAgreement();
    
         Test.startTest();
           c.start(null);
           c.execute(null,agreement); 
           c.finish(null);  
         Test.StopTest();
   
   }
}